<?php
return [
    'configs' => [
        'rbs_selifa_data_dbi' => [
            'DefaultDatabase' => 'Default',
            'DisableError' => false,
            'DisableLastQueryOnError' => false,
            'Connections' => [
                'Default' => [
                    'Driver' => 'mysql',
                    'Host' => 'localhost:3306',
                    'Name' => 'testdb',
                    'User' => 'root',
                    'Password' => '',
                    'Extension' => '',
                    'ConnectionClass' => '',
                    'TableObjectClass' => '',
                    'QueryObjectClass' => '',
                    'ProcedureObjectClass' => '',
                    'DataTableClass' => '',
                    'ReadOnlyRowClass' => '',
                    'WritableRowClass' => ''
                ]
            ]
        ]
    ],
    'command-packages' => [
        'RBS\Selifa\Data\CLI\DataCommandPackage'
    ]
];
?>