<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Driver;
use RBS\Selifa\Data\Feature\DatabaseSchemaPreProcessTrait;
use RBS\Selifa\Data\Interfaces\IDatabaseScriptGenerator;
use RBS\Selifa\Data\Feature\SchemaObject;
use RBS\Utility\F;
use Exception;

/**
 * Class mysqlScriptGenerator
 *
 * @package RBS\Selifa\Data\Driver
 */
class mysqlScriptGenerator implements IDatabaseScriptGenerator
{
    use DatabaseSchemaPreProcessTrait;

    /**
     * @param SchemaObject $schema
     */
    public function Initialize(SchemaObject $schema)
    {
        $this->SetSchemaObject($schema);
        $this->ParseSchemaSpec();
    }

    /**
     * @return string
     */
    public function CreateDatabaseInitialization()
    {
        $s  = "DROP SCHEMA IF EXISTS `".$this->_DBName."`;\n";
        $s .= "CREATE SCHEMA IF NOT EXISTS `".$this->_DBName."`;\n";
        $s .= "USE `".$this->_DBName."`;\n\n";

        $createUser = (($this->_DBUserName != '') && ($this->_DBPassword != ''));
        if ($createUser)
        {
            $s .= "CREATE USER `".$this->_DBUserName."`@`%` IDENTIFIED BY '".$this->_DBPassword."';\n";
            $s .= "GRANT ALL ON `".$this->_DBName."`.* TO `".$this->_DBUserName."`@`%`;\n";
        }

        return ($s."\n");
    }

    /**
     * @param string $tableName
     * @param bool $skipUpdateLog
     * @param bool $triggerOnly
     * @return string
     * @throws Exception;
     */
    public function CreateTable($tableName,$skipUpdateLog=false,$triggerOnly=false)
    {
        if (!isset($this->_Tables[$tableName]))
            throw new Exception('Table '.$tableName.' does not exists in specification.');
        $spec = $this->_Tables[$tableName];

        $insertLt = (bool)F::Extract($spec,'InsertLogTrigger',false);
        $updateLt = (bool)F::Extract($spec,'UpdateLogTrigger',false);
        $deleteLt = (bool)F::Extract($spec,'DeleteLogTrigger',false);

        $doDropTable = (bool)F::Extract($spec,'DropTable',true);
        $tableName = trim(F::Extract($spec,'Name','_Unknown'));

        $uCols = [];

        $s = '';
        if ($doDropTable)
            $s .= "DROP TABLE IF EXISTS `".$tableName."`;\n";

        $s .= "/*!40101 SET @saved_cs_client     = @@character_set_client */;\n";
        $s .= "/*!40101 SET character_set_client = utf8 */;\n";
        $s .= "CREATE TABLE `".$tableName."` (\n";

        $idType = trim(F::Extract($spec,'IDType'));
        $idLength = F::Extract($spec,'IDLength',null);
        $idAutoInc = (bool)F::Extract($spec,'IDAutoIncrement',true);
        $s .= "\t`ID` ".$idType.($idLength !== null?'('.$idLength.')':'').' NOT NULL'.($idAutoInc?' AUTO_INCREMENT':'').",\n";

        $changeMeta = (bool)F::Extract($spec,'ChangeMeta',false);
        if ($changeMeta)
        {
            $s .= "\t`Created` DATETIME NOT NULL,\n";
            $s .= "\t`Modified` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,\n";
            $s .= "\t`CUID` INTEGER UNSIGNED NOT NULL DEFAULT 0,\n";
            $s .= "\t`MUID` INTEGER UNSIGNED NOT NULL DEFAULT 0,\n";

            $uCols['Modified'] = 'TIMESTAMP';
            $uCols['MUID'] = 'INTEGER';
        }

        $softDelete = (bool)F::Extract($spec,'SoftDelete',false);
        if ($softDelete)
            $s .= "\t`".$this->_DeleteFlagName."` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,\n";

        $softActive = (bool)F::Extract($spec,'SoftActive',false);
        $softActiveIdx = (bool)F::Extract($spec,'SoftActiveIndexed',false);
        if ($softActive)
            $s .= "\t`".$this->_ActiveFlagName."` TINYINT(1) UNSIGNED NOT NULL DEFAULT 1,\n";

        $indexes = [];
        foreach ($spec['Columns'] as $col)
        {
            $colName = trim(F::Extract($col,'Name','_Unknown'));
            $type = strtoupper(trim(F::Extract($col,'Type','VARCHAR')));
            $length = F::Extract($col,'Length',null);
            $isNullable = (bool)F::Extract($col,'Nullable',true);
            $intSign = strtoupper(trim(F::Extract($col,'Sign','')));
            $defValue = F::Extract($col,'Default',null);
            $useIndex = (bool)F::Extract($col,'Indexed',false);
            $skipTrigger = (bool)F::Extract($col,'SkipTrigger',false);

            if (is_string($defValue))
            {
                if (!in_array($defValue,['CURRENT_TIMESTAMP']))
                    $defValue = ("'".$defValue."'");
            }

            $s .= "\t`".$colName."` ".$type.($length !== null?'('.$length.')':'').($intSign != ''?' '.$intSign:'');
            $s .= (!$isNullable?' NOT NULL':'').($defValue !== null?' DEFAULT '.$defValue:'').",\n";

            if ($useIndex)
            {
                $idxCols = [];
                if ($softDelete)
                    $idxCols[] = "`".$this->_DeleteFlagName."`";
                if ($softActive)
                {
                    if ($softActiveIdx)
                        $idxCols[] = "`".$this->_ActiveFlagName."`";
                }
                $idxCols[] = "`".$colName."`";
                $indexes[] = "\tKEY `IDX_".$tableName.'_'.$colName."`(".implode(',',$idxCols).")";
            }

            if (!$skipTrigger)
                $uCols[$colName] = $type;
        }

        $specIndexes = F::Extract($spec,'Indexes',[]);
        foreach ($specIndexes as $sdx)
        {
            $indexName = trim(F::Extract($sdx,'Name','_UnknownIndex'));
            $incSoftDelete = (bool)F::Extract($sdx,'IncludeSoftDelete',false);
            $incSoftActive = (bool)F::Extract($sdx,'IncludeSoftActive',false);

            $idxCols = [];
            if ($softDelete && $incSoftDelete)
                $idxCols[] = "`".$this->_DeleteFlagName."`";
            if ($softActive && $incSoftActive)
                $idxCols[] = "`".$this->_ActiveFlagName."`";
            foreach ($sdx['Columns'] as $colName)
                $idxCols[] = "`".$colName."`";

            $indexes[] = "\tKEY `IDX_".$tableName.'_'.$indexName."`(".implode(',',$idxCols).")";
        }

        $s .= "\tPRIMARY KEY (`ID`)".(count($indexes) > 0?',':'')."\n";
        $s .= implode(",\n",$indexes);
        $s .= (count($indexes) > 0?"\n":'');

        $engine = trim(F::Extract($spec,'Engine','MyISAM'));
        $charset = strtolower(trim(F::Extract($spec,'Charset','utf8')));
        $stAutoInc = (int)F::Extract($spec,'AutoIncrementStart',1);
        $s .= ") ENGINE=".$engine." AUTO_INCREMENT=".$stAutoInc." DEFAULT CHARSET=".$charset.";\n";
        $s .= "/*!40101 SET character_set_client = @saved_cs_client */;\n\n";

        if ($triggerOnly)
            $s = '';

        if ((!$skipUpdateLog) && ($tableName != $this->_RULTableName))
        {
            if ($insertLt)
            {
                $iTrigName = 'TGR_'.$tableName.'_AfterInsert_RUL';
                $i  = "DROP TRIGGER /*!50030 IF EXISTS */ `".$iTrigName."`;\n";
                $i .= "DELIMITER $$\n";
                $i .= "CREATE TRIGGER `".$iTrigName."` AFTER INSERT ON `".$tableName."` FOR EACH ROW\n";
                $i .= "BEGIN\n";
                $i .= "\tINSERT INTO `".$this->_RULTableName."`(`DBUser`,`UserID`,`TableName`,`RowID`,`EventType`,`Data`)\n";
                $i .= "\t\tVALUES(CURRENT_USER(),NEW.CUID,'".$tableName."',NEW.ID,'INSERT','');\n";
                $i .= "END $$\nDELIMITER ;\n";
                $s .= ($i."\n");
            }

            if ($updateLt)
            {
                if ($changeMeta)
                {
                    $bTrigName = 'TGR_'.$tableName.'_BeforeUpdate_RUL';
                    $b  = "DROP TRIGGER /*!50030 IF EXISTS */ `".$bTrigName."`;\n";
                    $b .= "DELIMITER $$\n";
                    $b .= "CREATE TRIGGER `".$bTrigName."` BEFORE UPDATE ON `".$tableName."` FOR EACH ROW\n";
                    $b .= "BEGIN\n";
                    $b .= "\tIF (NEW.Created <> OLD.Created) THEN\n\t\tSET NEW.Created = OLD.Created;\n\tEND IF;\n";
                    $b .= "\tIF (NEW.CUID <> OLD.CUID) THEN\n\t\tSET NEW.CUID = OLD.CUID;\n\tEND IF;\n";
                    $b .= "END $$\nDELIMITER ;\n";
                    $s .= ($b."\n\n");
                }

                $uTrigName = 'TGR_'.$tableName.'_AfterUpdate_RUL';
                $u  = "DROP TRIGGER /*!50030 IF EXISTS */ `".$uTrigName."`;\n";
                $u .= "DELIMITER $$\n";
                $u .= "CREATE TRIGGER `".$uTrigName."` AFTER UPDATE ON `".$tableName."` FOR EACH ROW\n";
                $u .= "BEGIN\n\tDECLARE _Temp TEXT;\n\tSET _Temp = '';\n";

                if ($softDelete)
                {
                    $dfName = $this->_DeleteFlagName;
                    $u .= "\tIF (NEW.".$dfName." <> OLD.".$dfName.") THEN\n";
                    $u .= "\t\tSET _Temp = CONCAT(_Temp,'\"".$dfName."\": [',OLD.".$dfName.",',',NEW.".$dfName.",'],');\n";
                    $u .= "\tEND IF;\n";
                }

                if ($softActive)
                {
                    $afName = $this->_ActiveFlagName;
                    $u .= "\tIF (NEW.".$afName." <> OLD.".$afName.") THEN\n";
                    $u .= "\t\tSET _Temp = CONCAT(_Temp,'\"".$afName."\": [',OLD.".$afName.",',',NEW.".$afName.",'],');\n";
                    $u .= "\tEND IF;\n";
                }

                foreach ($spec['Columns'] as $col)
                {
                    $skipTrigger = (bool)F::Extract($col,'SkipTrigger',false);
                    if ($skipTrigger)
                        continue;

                    $colName = trim(F::Extract($col,'Name','_Unknown'));
                    $type = strtoupper(trim(F::Extract($col,'Type','VARCHAR')));
                    $isNullable = (bool)F::Extract($col,'Nullable',true);

                    if ($isNullable)
                    {
                        $u .= "\tIF (NOT(NEW.".$colName." <=> OLD.".$colName.")) THEN\n";
                        if (in_array($type,$this->_NumericTypes))
                            $u .= "\t\tSET _Temp = CONCAT(_Temp,'\"".$colName."\": [',IFNULL(OLD.".$colName.",'null'),',',IFNULL(NEW.".$colName.",'null'),'],');\n";
                        else
                            $u .= "\t\tSET _Temp = CONCAT(_Temp,'\"".$colName."\": [',IF(OLD.".$colName." IS NULL,'null',CONCAT('\"',OLD.".$colName.",'\"')),',',IF(NEW.".$colName." IS NULL,'null',CONCAT('\"',NEW.".$colName.",'\"')),'],');\n";
                    }
                    else
                    {
                        $u .= "\tIF (NEW.".$colName." <> OLD.".$colName.") THEN\n";
                        if (in_array($type,$this->_NumericTypes))
                            $u .= "\t\tSET _Temp = CONCAT(_Temp,'\"".$colName."\": [',OLD.".$colName.",',',NEW.".$colName.",'],');\n";
                        else
                            $u .= "\t\tSET _Temp = CONCAT(_Temp,'\"".$colName."\": [\"',OLD.".$colName.",'\",\"',NEW.".$colName.",'\"],');\n";
                    }
                    $u .= "\tEND IF;\n";
                }

                $u .= "\tSET _Temp = CONCAT(_Temp,'\"__TYPE\": \"UPDATE\"');\n";
                $u .= "\tSET _Temp = CONCAT('{',_Temp,'}');\n\n";
                $u .= "\tINSERT INTO `".$this->_RULTableName."`(`DBUser`,`UserID`,`TableName`,`RowID`,`EventType`,`Data`)\n";
                $u .= "\t\tVALUES(CURRENT_USER(),NEW.MUID,'".$tableName."',NEW.ID,'UPDATE',_Temp);\n";
                $u .= "END $$\nDELIMITER ;\n";
                $s .= ($u."\n");
            }

            if ($deleteLt)
            {
                $uTrigName = 'TGR_'.$tableName.'_AfterDelete_RUL';
                $d  = "DROP TRIGGER /*!50030 IF EXISTS */ `".$uTrigName."`;\n";
                $d .= "DELIMITER $$\n";
                $d .= "CREATE TRIGGER `".$uTrigName."` AFTER DELETE ON `".$tableName."` FOR EACH ROW\n";
                $d .= "BEGIN\n\tDECLARE _Temp TEXT;\n\tSET _Temp = '';\n";

                if ($softDelete)
                {
                    $dfName = $this->_DeleteFlagName;
                    $d .= "\tSET _Temp = CONCAT(_Temp,'\"".$dfName."\": [',OLD.".$dfName.",'],');\n";
                }

                if ($softActive)
                {
                    $afName = $this->_ActiveFlagName;
                    $d .= "\tSET _Temp = CONCAT(_Temp,'\"".$afName."\": [',OLD.".$afName.",'],');\n";
                }

                foreach ($spec['Columns'] as $col)
                {
                    $skipTrigger = (bool)F::Extract($col,'SkipTrigger',false);
                    if ($skipTrigger)
                        continue;

                    $colName = trim(F::Extract($col,'Name','_Unknown'));
                    $type = strtoupper(trim(F::Extract($col,'Type','VARCHAR')));
                    $isNullable = (bool)F::Extract($col,'Nullable',true);

                    if ($isNullable)
                    {
                        if (in_array($type,$this->_NumericTypes))
                            $d .= "\tSET _Temp = CONCAT(_Temp,'\"".$colName."\": [',IFNULL(OLD.".$colName.",'null'),'],');\n";
                        else
                            $d .= "\tSET _Temp = CONCAT(_Temp,'\"".$colName."\": [',IF(OLD.".$colName." IS NULL,'null',CONCAT('\"',OLD.".$colName.",'\"')),'],');\n";
                    }
                    else
                    {
                        if (in_array($type,$this->_NumericTypes))
                            $d .= "\tSET _Temp = CONCAT(_Temp,'\"".$colName."\": [',OLD.".$colName.",'],');\n";
                        else
                            $d .= "\tSET _Temp = CONCAT(_Temp,'\"".$colName."\": [\"',OLD.".$colName.",'\"],');\n";
                    }
                }

                $d .= "\tSET _Temp = CONCAT(_Temp,'\"__TYPE\": \"DELETE\"');\n";
                $d .= "\tSET _Temp = CONCAT('{',_Temp,'}');\n\n";
                $d .= "\tINSERT INTO `".$this->_RULTableName."`(`DBUser`,`UserID`,`TableName`,`RowID`,`EventType`,`Data`)\n";
                $d .= "\t\tVALUES(CURRENT_USER(),0,'".$tableName."',OLD.ID,'DELETE',_Temp);\n";
                $d .= "END $$\nDELIMITER ;\n";
                $s .= ($d."\n");
            }
        }

        if (isset($spec['PostScript']))
        {
            foreach ($spec['PostScript'] as $scr)
                $s .= ($scr."\n\n");
        }
        return $s;
    }

    /**
     * @param bool $skipUpdateLog
     * @return string
     * @throws Exception
     */
    public function CreateTables($skipUpdateLog = false)
    {
        $s = '';
        foreach ($this->_PreScripts as $scr)
            $s .= ($scr."\n\n");
        foreach ($this->_TableNames as $tName)
            $s .= ($this->CreateTable($tName,$skipUpdateLog)."\n");
        foreach ($this->_PostScripts as $scr)
            $s .= ("\n".$scr."\n");
        return $s;
    }

    /**
     * @param bool $skipUpdateLog
     * @param bool $triggerOnly
     * @return string
     * @throws Exception
     */
    public function CreateFull($skipUpdateLog=false,$triggerOnly=false)
    {
        if (!$triggerOnly)
        {
            $s = $this->CreateDatabaseInitialization();
            foreach ($this->_PreScripts as $scr)
                $s .= ($scr."\n\n");
        }
        else
            $s = '';

        foreach ($this->_TableNames as $tName)
            $s .= ($this->CreateTable($tName,$skipUpdateLog,$triggerOnly)."\n");

        if (!$triggerOnly)
        {
            foreach ($this->_PostScripts as $scr)
                $s .= ("\n".$scr."\n");
        }

        return $s;
    }

    /**
     * @param string $tableName
     * @return string
     * @throws Exception
     */
    public function CreateBatchInsertTemplate($tableName)
    {
        if (!isset($this->_Tables[$tableName]))
            throw new Exception('Table '.$tableName.' does not exists in specification.');
        $spec = $this->_Tables[$tableName];

        $tableName = trim(F::Extract($spec,'Name','_Unknown'));
        $changeMeta = (bool)F::Extract($spec,'ChangeMeta',false);
        $softDelete = (bool)F::Extract($spec,'SoftDelete',false);
        $softActive = (bool)F::Extract($spec,'SoftActive',false);

        $incCols = ['ID'];
        if ($changeMeta)
            $incCols = array_merge_recursive($incCols,['Created','CUID','MUID']);
        if ($softDelete)
            $incCols[] = $this->_DeleteFlagName;
        if ($softActive)
            $incCols[] = $this->_ActiveFlagName;

        foreach ($spec['Columns'] as $col)
        {
            $colName = trim(F::Extract($col,'Name','_Unknown'));
            $incCols[] = $colName;
        }

        $cols = [];
        foreach ($incCols as $colName)
            $cols[] = ("`".$colName."`");

        $s  = "LOCK TABLES `".$tableName."` WRITE;\n";
        $s .= "/*!40000 ALTER TABLE `".$tableName."` DISABLE KEYS */;\n";
        $s .= "INSERT INTO `".$tableName."` (".implode(',',$cols).") VALUES\n";
        $s .= "\t();\n";
        $s .= "/*!40000 ALTER TABLE `".$tableName."` ENABLE KEYS */;\n";
        $s .= "UNLOCK TABLES;\n";

        return $s;
    }

    /**
     * @return string
     */
    public function CreateBatchInsertTemplates()
    {
        $s = '';
        foreach ($this->_TableNames as $tName)
            $s .= ($this->CreateBatchInsertTemplateForTable($tName)."\n");
        return $s;
    }
}
?>