<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Driver;
use RBS\Selifa\Data\DBI;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use Exception;
use DateTime;
use RBS\Selifa\Data\BaseDialect;

/**
 * Class pgsqlDriver
 *
 * @package RBS\Selifa\Data\Driver
 */
class pgsqlDriver implements IDatabaseDriver
{
    /**
     * @var resource|bool|null
     */
    private $ConnectionInstance = null;

    /**
     * @var null|mysqlDialect;
     */
    private $_Dialect = null;

    /**
     * @var string
     */
    private $_DatabaseName = '';

    /**
     * @var string
     */
    private $_LastQuery = '';

    /**
     * @param string $typeString
     * @return string
     */
    protected function PGTypeToDBIType($typeString)
    {
        $aType = strtolower(trim($typeString));
        if ($aType[0] == '_')
            return DBI_FT_ARRAY;

        switch ($typeString)
        {
            case 'bit':
            case 'boolean':
            case 'bool':
                return DBI_FT_BOOLEAN;
            case 'smallint':
            case 'smallserial':
            case 'integer':
            case 'serial':
            case 'int4':
            case 'int4range':
            case 'bigint':
            case 'bigserial':
            case 'int8':
            case 'int8range':
                return DBI_FT_INTEGER;
            case 'date':
            case 'time':
            case 'time with time zone':
            case 'time without time zone':
            case 'interval':
                return DBI_FT_DATETIME;
            case 'timestamp':
            case 'timestamp without time zone':
            case 'timestamp with time zone':
                return DBI_FT_TIMESTAMP;
            case 'real':
            case 'double precision':
            case 'money':
            case 'decimal':
            case 'numeric':
            case 'numrange':
            case 'float4':
            case 'float8':
                return DBI_FT_REAL;
            case 'character':
            case 'char':
            case 'text':
            case 'varchar':
            case 'character varying':
                return DBI_FT_VARCHAR;
            case 'tiny_blob':
            case 'medium_blob':
            case 'long_blob':
            case 'blob':
                return DBI_FT_BLOB;
            case 'json':
                return DBI_FT_JSON;
            default:
                return DBI_FT_VARCHAR;
        }
    }

    /**
     * @param array $connectionSpec
     * @throws Exception
     */
    public function Initialize($connectionSpec)
    {
        if (!function_exists('pg_connect'))
            DBI::Instance()->ThrowDatabaseException('PostgreSQL extension is not installed or enabled.');

        $hosts = explode(':', $connectionSpec['host'], 2);
        if (count($hosts) > 1)
            $cString = ("host=".$hosts[0]." port=".$hosts[1]." dbname=".$connectionSpec['name']);
        else
            $cString = ("host=".$hosts[0]." dbname=".$connectionSpec['name']);
        if (isset($connectionSpec['user']))
            if (trim($connectionSpec['user']) != '')
                $cString .= " user=".trim($connectionSpec['user']);
        if (isset($connectionSpec['pass']))
            if (trim($connectionSpec['pass']) != '')
                $cString .= " password=".trim($connectionSpec['pass']);
        $this->_DatabaseName = $connectionSpec['name'];

        $this->ConnectionInstance = @pg_connect($cString,PGSQL_CONNECT_FORCE_NEW);
        if ($this->ConnectionInstance === false)
            DBI::Instance()->ThrowDatabaseException('Could not connect to PostgreSQL server.');
    }

    /**
     * @return BaseDialect
     */
    public function GetDialect()
    {
        if ($this->_Dialect === null)
            $this->_Dialect = new pgsqlDialect();
        return $this->_Dialect;
    }

    /**
     * @return bool
     */
    public function IsMultiQuerySupported()
    {
        return false;
    }

    /**
     * @return string
     */
    public function GetDatabaseName()
    {
        return $this->_DatabaseName;
    }

    /**
     * @return string
     */
    public function GetDriverType()
    {
        return 'pgsql';
    }

    /**
     * @param string $name
     * @param array $parameters
     * @return mixed
     */
    public function StoredProcedureSQL($name, $parameters)
    {
        if (count($parameters) > 0)
        {
            $s = '';
            foreach ($parameters as $item)
            {
                if ($item === null)
                    $s .= (",NULL");
                else if ($item instanceof DateTime)
                    $s .= (",'".$item->format('Y-m-d H:i:s')."''");
                else if (is_string($item))
                    $s .= (",'".$this->EscapeString($item)."'");
                else
                    $s .= (','.$this->EscapeString($item));
            }
            $s[0] = '(';
        }
        else
            $s = '(';
        return ('CALL '.$name.$s.')');
    }

    /**
     * @param resource|mixed $result
     * @return mixed
     */
    public function Fetch($result)
    {
        $return = pg_fetch_array($result);
        if ($return === false)
            return NULL;
        return $return;
    }

    /**
     * @param resource|mixed $result
     */
    public function FreeResult($result)
    {
        pg_free_result($result);
    }

    /**
     * @param resource|mixed $result
     * @return int
     */
    public function GetRowCount($result)
    {
        return (int)pg_num_rows($result);
    }

    /**
     * @param string $query
     * @return resource|bool
     */
    public function Query($query)
    {
        $this->_LastQuery = $query;
        return pg_query($this->ConnectionInstance,$query);
    }

    /**
     * @param string $query
     * @return resource|bool
     */
    public function SingleQuery($query)
    {
        return pg_query($this->ConnectionInstance,$query);
    }

    /**
     * @param resource $dbResource
     * @param string $tableName
     * @return array
     */
    public function GetResultMetadata($dbResource, $tableName)
    {
        $result = array(
            'TableName' => $tableName,
            'ColumnInfo' => array(),
            'IsPrimaryKeyAvailable' => false,
            'PrimaryKeys' => array()
        );

        if ($tableName != '')
        {
            $sql = "SELECT a.attname, format_type(a.atttypid, a.atttypmod) AS data_type
                    FROM   pg_index i
                    JOIN   pg_attribute a ON a.attrelid = i.indrelid
                                         AND a.attnum = ANY(i.indkey)
                    WHERE  i.indrelid = '".$tableName."'::regclass
                    AND    i.indisprimary";
            $kResult = pg_query($this->ConnectionInstance,$sql);

            $keys = array();
            while ($row = pg_fetch_assoc($kResult))
                $keys[] = trim($row['attname']);

            pg_free_result($kResult);

            $dialect = $this->_Dialect;
            $tnSplits = explode('.',$tableName,2);
            if (count($tnSplits) > 1)
            {
                $sql = "SELECT column_name, column_default, is_nullable, data_type, udt_name
                    FROM information_schema.columns
                    WHERE (table_schema = '".$tnSplits[0]."') AND (table_name = '".$tnSplits[1]."')";
            }
            else
            {
                $tName = ($dialect->S.$tableName.$dialect->E);
                $sql = "SELECT column_name, column_default, is_nullable, data_type, udt_name
                    FROM information_schema.columns
                    WHERE (table_name = '".$tName.")";
            }

            $qResult = pg_query($this->ConnectionInstance,$sql);
            while ($row = pg_fetch_assoc($qResult))
            {
                $info = array();
                $fieldName = trim($row['column_name']);
                $fieldType = trim($row['udt_name']);

                $info['value'] = null;
                $info['name'] = $fieldName;
                $info['type_o'] = $fieldType;
                $info['type'] = $this->PGTypeToDBIType($fieldType);

                $info['key'] = (in_array($fieldName,$keys));
                $info['auto'] = (strpos($row['column_default'],'nextval') > -1);

                if ($info['type'] == DBI_FT_TIMESTAMP)
                    $info['auto'] = true;

                $info['modified'] = false;
                $info['forcenull'] = false;

                if ($info['key'])
                    $result['PrimaryKeys'][] = $info['name'];

                $result['ColumnInfo'][$fieldName] = $info;
            }

            pg_free_result($qResult);
        }
        else
        {
            $numFields = pg_num_fields($dbResource);
            for ($i=0;$i<$numFields;$i++)
            {
                $info = array();
                $fieldName = pg_field_name($dbResource,$i);
                $fieldType = pg_field_type($dbResource,$i);

                $info['value'] = null;
                $info['name'] = $fieldName;
                $info['type_o'] = $fieldType;
                $info['type'] = $this->PGTypeToDBIType($fieldType);

                $info['key'] = false;
                $info['auto'] = false;

                if ($info['type'] == DBI_FT_TIMESTAMP)
                    $info['auto'] = true;

                $info['modified'] = false;
                $info['forcenull'] = false;

                if ($info['key'])
                    $result['PrimaryKeys'][] = $info['name'];

                $result['ColumnInfo'][$fieldName] = $info;
            }
        }

        $result['IsPrimaryKeyAvailable'] = (count($result['PrimaryKeys']) > 0);
        return $result;
    }

    /**
     * @param string $input
     * @return string
     */
    public function EscapeString($input)
    {
        return pg_escape_string($this->ConnectionInstance,$input);
    }

    /**
     *
     */
    public function Close()
    {
        if ($this->ConnectionInstance !== null)
        {
            pg_close($this->ConnectionInstance);
            $this->ConnectionInstance = null;
        }
    }

    public function LastInsertedIdentifier()
    {
        // TODO: Implement LastInsertedIdentifier() method.
    }

    public function HasError()
    {
        // TODO: Implement HasError() method.
    }

    public function GetResult()
    {
        // TODO: Implement GetResult() method.
    }

    public function HasMoreResult()
    {
        // TODO: Implement HasMoreResult() method.
    }

    public function NextResult()
    {
        // TODO: Implement NextResult() method.
    }

    public function TransactionStart()
    {
        // TODO: Implement TransactionStart() method.
    }

    public function TransactionSave($id)
    {
        // TODO: Implement TransactionSave() method.
    }

    public function TransactionBackTo($id)
    {
        // TODO: Implement TransactionBackTo() method.
    }

    public function TransactionDelete($id)
    {
        // TODO: Implement TransactionDelete() method.
    }

    public function TransactionCommit()
    {
        // TODO: Implement TransactionCommit() method.
    }

    public function TransactionRollback()
    {
        // TODO: Implement TransactionRollback() method.
    }
}
?>