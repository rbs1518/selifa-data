<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Driver;
use RBS\Selifa\Data\DBI;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use Exception;
use mysqli;
use mysqli_result;
use DateTime;
use RBS\Selifa\Data\BaseDialect;

/**
 * Class mysqlDriver
 *
 * @package RBS\Selifa\Data\Driver
 */
class mysqlDriver implements IDatabaseDriver
{
    /**
     * @var resource|bool|null
     */
    private $ConnectionInstance = null;

    /**
     * @var null|mysqlDialect;
     */
    private $_Dialect = null;

    /**
     * @var string
     */
    private $_DatabaseName = '';

    /**
     * @var string
     */
    private $_LastQuery = '';

    /**
     * @var array
     */
    private $_Types = array();

    /**
     * @var array
     */
    private $_Flags = array();

    /**
     * @return array
     */
    protected function GetMySQLiTypes()
    {
        $types = array();
        $constants = get_defined_constants(true);
        foreach ($constants['mysqli'] as $c => $n)
        {
            if (preg_match('/^MYSQLI_TYPE_(.*)/', $c, $m))
            {
                $types[$n] = strtolower($m[1]);
            }
        }

        return $types;
    }

    /**
     * @return array
     */
    protected function GetMySQLiFlags()
    {
        $flags = array();
        $constants = get_defined_constants(true);
        foreach ($constants['mysqli'] as $c => $n)
        {
            if (preg_match('/MYSQLI_(.*)_FLAG$/', $c, $m))
            {
                if (!array_key_exists($n, $flags))
                {
                    $flags[$n] = $m[1];
                }
            }
        }

        return $flags;
    }

    /**
     * @param array $typeArray
     * @param int $sType
     * @return string
     */
    protected function MySQLiTypeToDBIType($typeArray, $sType)
    {
        $type = $typeArray[$sType];
        switch ($type)
        {
            case 'char':
            case 'short':
            case 'long':
            case 'int24':
            case 'longlong':
            case 'year':
                return DBI_FT_INTEGER;
            case 'datetime':
            case 'date':
            case 'time':
            case 'newdate':
            case 'interval':
                return DBI_FT_DATETIME;
            case 'timestamp':
                return DBI_FT_TIMESTAMP;
            case 'float':
            case 'double':
            case 'decimal':
            case 'newdecimal':
                return DBI_FT_REAL;
            case 'var_string':
            case 'string':
                return DBI_FT_VARCHAR;
            case 'tiny_blob':
            case 'medium_blob':
            case 'long_blob':
            case 'blob':
                return DBI_FT_BLOB;
        }

        return '';
    }

    /**
     * @param array|string $flags
     * @param int $flagNo
     * @return array
     */
    protected function MySQLiAppliedFlags($flags, $flagNo)
    {
        $result = array();
        foreach ($flags as $n => $t)
        {
            if ($flagNo & $n)
            {
                $result[] = $t;
            }
        }
        return $result;
    }

    /**
     * @param string $message
     * @param int $code
     * @throws Exception
     */
    private function _ThrowDBError($message, $code = DB_GENERAL_ERROR)
    {
        $clientError = $this->ConnectionInstance->error;
        if ($clientError != '')
            $eMessage = ($clientError . ($message != '' ? ' - ' . $message : ''));
        else
            $eMessage = $message;
        DBI::Instance()->ThrowDatabaseException($eMessage,$code);
    }

    /**
     * @param array $connectionSpec
     * @throws Exception
     */
    public function Initialize($connectionSpec)
    {
        if (isset($connectionSpec['host']))
            $sHost = trim($connectionSpec['host']);
        else if (isset($connectionSpec['Host']))
            $sHost = trim($connectionSpec['Host']);
        else
            $sHost = 'localhost:3306';
        $hosts = explode(':',$sHost,2);

        if (isset($connectionSpec['user']))
            $aUser = trim($connectionSpec['user']);
        else if (isset($connectionSpec['User']))
            $aUser = trim($connectionSpec['User']);
        else
            $aUser = 'root';

        if (isset($connectionSpec['pass']))
            $aPass = trim($connectionSpec['pass']);
        else if (isset($connectionSpec['Password']))
            $aPass = trim($connectionSpec['Password']);
        else
            $aPass = '';

        if (isset($connectionSpec['name']))
            $aName = trim($connectionSpec['name']);
        else if (isset($connectionSpec['Name']))
            $aName = trim($connectionSpec['Name']);
        else
            $aName = 'mysql';

        if (count($hosts) > 1)
            $this->ConnectionInstance = @new mysqli($hosts[0],$aUser,$aPass,$aName,$hosts[1]);
        else
            $this->ConnectionInstance = @new mysqli($hosts[0],$aUser,$aPass,$aName);
        $this->_DatabaseName = $aName;

        if ($this->ConnectionInstance->connect_error)
        {
            $error  = 'Could not connect to MySQL server ('.$aName.') using `'.$aUser.'`@`'.$sHost.'`.';
            $error .= ' Client returned '.$this->ConnectionInstance->connect_errno.': ';
            $error .= $this->ConnectionInstance->connect_error;
            $this->_ThrowDBError($error);
        }

        $this->_Types = $this->GetMySQLiTypes();
        $this->_Flags = $this->GetMySQLiFlags();
    }

    /**
     * @return BaseDialect
     */
    public function GetDialect()
    {
        if ($this->_Dialect === null)
            $this->_Dialect = new mysqlDialect();
        return $this->_Dialect;
    }

    /**
     * @return bool
     */
    public function IsMultiQuerySupported()
    {
        return true;
    }

    /**
     * @return string
     */
    public function GetDatabaseName()
    {
        return $this->_DatabaseName;
    }

    /**
     * @return string
     */
    public function GetDriverType()
    {
        return 'mysql';
    }

    /**
     * @internal
     * @param string $name
     * @param array $parameters
     * @return mixed
     */
    public function StoredProcedureSQL($name, $parameters)
    {
        if (count($parameters) > 0)
        {
            $s = '';
            foreach ($parameters as $item)
            {
                if ($item === null)
                    $s .= (",NULL");
                else if ($item instanceof DateTime)
                    $s .= (",'".$item->format('Y-m-d H:i:s')."''");
                else if (is_string($item))
                    $s .= (",'".$this->EscapeString($item)."'");
                else
                    $s .= (','.$this->EscapeString($item));
            }
            $s[0] = '(';
        }
        else
            $s = '(';
        return ('CALL '.$name.$s.')');
    }

    /**
     * @internal
     * @param mysqli_result|resource|mixed $result
     * @return mixed
     */
    public function Fetch($result)
    {
        return $result->fetch_array(MYSQLI_ASSOC);
    }

    /**
     * @internal
     * @param mysqli_result|resource|mixed $result
     */
    public function FreeResult($result)
    {
        $result->free_result();
    }

    /**
     * @internal
     * @param mysqli_result|resource|mixed $result
     * @return int
     */
    public function GetRowCount($result)
    {
        return $result->num_rows;
    }

    /**
     * @internal
     * @param string $query
     * @return mysqli_result|resource|bool
     */
    public function Query($query)
    {
        $this->_LastQuery = $query;
        return $this->ConnectionInstance->multi_query($query);
    }

    /**
     * @internal
     * @param string $query
     * @return mysqli_result|resource|bool
     */
    public function SingleQuery($query)
    {
        $this->_LastQuery = $query;
        return $this->ConnectionInstance->query($query,MYSQLI_STORE_RESULT);
    }

    /**
     * @internal
     * @param mysqli_result|resource|null $dbResource
     * @param string $tableName
     * @return array
     */
    public function GetResultMetadata($dbResource,$tableName)
    {
        if ($tableName != '')
        {
            $sql = "select * from `".$tableName."` limit 0";
            $dbRes = $this->ConnectionInstance->query($sql,MYSQLI_STORE_RESULT);
            $fields = $dbRes->fetch_fields();
        }
        else
            $fields = $dbResource->fetch_fields();

        $result = array(
            'TableName' => $tableName,
            'ColumnInfo' => array(),
            'IsPrimaryKeyAvailable' => false,
            'PrimaryKeys' => array()
        );

        foreach ($fields as $field)
        {
            $info = array();

            $info['value'] = null;
            $info['name'] = $field->name;
            $info['type'] = $this->MySQLiTypeToDBIType($this->_Types, $field->type);
            $info['length'] = $field->length;

            $aFlags = $this->MySQLiAppliedFlags($this->_Flags, $field->flags);
            $info['unsigned'] = in_array('UNSIGNED',$aFlags);
            $info['key'] = in_array('PRI_KEY',$aFlags);
            $info['auto'] = in_array('AUTO_INCREMENT',$aFlags);

            if ($info['unsigned'] && ($info['length'] == 1) && in_array('NUM',$aFlags) && ($field->type == 1))
                $info['ptype'] = DBI_FT_BOOLEAN;
            else
                $info['ptype'] = $info['type'];

            if ($info['type'] == DBI_FT_TIMESTAMP)
                $info['auto'] = true;

            $info['modified'] = false;
            $info['forcenull'] = false;

            if ($info['key'])
                $result['PrimaryKeys'][] = $info['name'];

            $result['ColumnInfo'][$field->name] = $info;
        }

        $result['IsPrimaryKeyAvailable'] = (count($result['PrimaryKeys']) > 0);
        return $result;
    }

    /**
     * @param string $input
     * @return string
     */
    public function EscapeString($input)
    {
        return $this->ConnectionInstance->real_escape_string($input);
    }

    /**
     *
     */
    public function Close()
    {
        if ($this->ConnectionInstance !== null)
        {
            $this->ConnectionInstance->close();
            $this->ConnectionInstance = null;
        }
    }

    public function LastInsertedIdentifier()
    {
        $q = $this->ConnectionInstance->query('SELECT last_insert_id()');
        $r = $q->fetch_row();
        return $r[0];
    }

    /**
     * @return bool
     */
    public function HasError()
    {
        $clientError = $this->ConnectionInstance->error;
        return ($clientError != '');
    }

    /**
     * @return string
     */
    public function GetError()
    {
        $clientError = $this->ConnectionInstance->error;
        return trim($clientError);
    }

    /**
     * @return mysqli_result|resource|null
     */
    public function GetResult()
    {
        return $this->ConnectionInstance->store_result();
    }

    /**
     * @return bool
     */
    public function HasMoreResult()
    {
        return $this->ConnectionInstance->more_results();
    }

    /**
     * @return bool
     */
    public function NextResult()
    {
        return $this->ConnectionInstance->next_result();
    }

    /**
     *
     */
    public function TransactionStart()
    {
        $this->ConnectionInstance->begin_transaction();
    }

    /**
     * @param string $id
     */
    public function TransactionSave($id)
    {
        $this->ConnectionInstance->savepoint($id);
    }

    /**
     * @param string $id
     */
    public function TransactionBackTo($id)
    {

    }

    /**
     * @param string $id
     */
    public function TransactionDelete($id)
    {
        $this->ConnectionInstance->release_savepoint($id);
    }

    /**
     *
     */
    public function TransactionCommit()
    {
        $this->ConnectionInstance->commit();
    }

    /**
     *
     */
    public function TransactionRollback()
    {
        $this->ConnectionInstance->rollback();
    }
}
?>