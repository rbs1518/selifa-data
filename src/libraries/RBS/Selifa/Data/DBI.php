<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Template
{
    if (!defined('SELIFA_TGX'))
    {
        interface ITGXObjectRenderer{}
    }
}

namespace RBS\Selifa\Data
{
    use Exception;
    use SelifaException;
    use RBS\Selifa\Data\Interfaces\IDataConnection;
    use RBS\Selifa\Data\Interfaces\IDataExtension;
    use RBS\Selifa\Data\Standard\DataConnection;
    use RBS\Selifa\IComponent;
    use RBS\Selifa\Traits\OptionableSingleton;

    define('SELIFA_DATABASE_INTERFACE', 'v4.5');

    /*Database Interface Constants*/
    define('DBI_FT_INTEGER', 'int');
    define('DBI_FT_REAL', 'real');
    define('DBI_FT_VARCHAR', 'string');
    define('DBI_FT_BLOB', 'blob');
    define('DBI_FT_DATETIME', 'datetime');
    define('DBI_FT_TIMESTAMP', 'timestamp');
    define('DBI_FT_ARRAY','array');
    define('DBI_FT_COMPOSITE','composite');
    define('DBI_FT_RECORD','record');
    define('DBI_FT_ENUM','enum');
    define('DBI_FT_JSON','json');
    define('DBI_FT_BOOLEAN','bool');

    define('DB_DELETE', 'mxqownxuiqwebvryvbiwucnwo');
    define('DB_NULL', 'mcjhsiofkhsusikdjhsnbfryc');

    define('DB_ROW_MODE_INSERT', 88881);
    define('DB_ROW_MODE_UPDATE', 88882);
    define('DB_ROW_MODE_READ_ONLY', 88883);

    define('DML_INSERT', 991);
    define('DML_UPDATE', 992);
    define('DML_DELETE', 993);

    define('DBI_SQL_STATEMENT_RUN',0);
    define('DBI_SQL_STATEMENT_GET_DATA',1);

    /* Database Error Constants */
    define('DB_INVALID_DATABASE_CONNECTION', SELIFA_EXCEPTION_SYSTEM_START_CODE + 70);
    define('DB_DRIVER_NOT_EXISTS', SELIFA_EXCEPTION_SYSTEM_START_CODE + 71);
    define('DB_INVALID_CONFIGURATION', SELIFA_EXCEPTION_SYSTEM_START_CODE + 72);
    define('DB_GENERAL_ERROR', SELIFA_EXCEPTION_SYSTEM_START_CODE + 73);
    define('DB_CONNECT_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 74);
    define('DB_READ_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 75);
    define('DB_INSERT_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 76);
    define('DB_UPDATE_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 77);
    define('DB_DELETE_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 78);
    define('DB_DDL_FAILED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 79);
    define('DB_FOREIGN_KEY_VIOLATED', SELIFA_EXCEPTION_SYSTEM_START_CODE + 80);
    define('DB_KEY_ISNOT_UNIQUE', SELIFA_EXCEPTION_SYSTEM_START_CODE + 81);

    define('MODEL_VALIDATION_TYPE_INSERT',1);
    define('MODEL_VALIDATION_TYPE_UPDATE',2);

    define('CRUD_ACTION_TYPE_INSERT',1);
    define('CRUD_ACTION_TYPE_UPDATE',2);

    /**
     * Class Database
     * @package RBS\Selifa
     */
    class DBI implements IComponent
    {
        use OptionableSingleton;

        /**
         * @var string
         */
        private $_DefaultDBName = 'Default';

        /**
         * @var array
         */
        private $_Connections = array();

        /**
         * @var IDataConnection[]
         */
        private $_Database = array();

        /**
         * @var bool
         */
        private $_DisableError = false;

        /**
         * @var bool
         */
        private $_DisableLastQueryOnError = false;

        /**
         * @var IDataExtension[]
         */
        private $_Extensions = [];

        /**
         * @param array $options
         */
        protected function _OnInit($options)
        {
            if (isset($options['DefaultDatabase']))
                $this->_DefaultDBName = $options['DefaultDatabase'];
            if (isset($options['DisableError']))
                $this->_DisableError = (bool)$options['DisableError'];
            if (isset($options['DisableLastQueryOnError']))
                $this->_DisableLastQueryOnError = (bool)$options['DisableLastQueryOnError'];
            if (isset($options['Connections']))
                $this->_Connections = $options['Connections'];
        }

        /**
         * @param string $dbName
         * @return IDataConnection
         * @throws Exception
         */
        private function _LoadDriverAndCreateConnectionInterface($dbName)
        {
            if (!isset($this->_Connections[$dbName]))
                throw new SelifaException(DB_INVALID_DATABASE_CONNECTION,'Invalid database connection name ('.$dbName.').');
            $dbConfig = $this->_Connections[$dbName];

            $driverNs = "RBS\\Selifa\\Data\\Driver\\";
            if (isset($dbConfig['Driver']))
            {
                $caDriver = trim($dbConfig['Driver']);
                if (strpos($caDriver,"\\",0) > -1)
                    $className = $caDriver;
                else
                    $className = ($driverNs.strtolower($caDriver).'Driver');
            }
            else if (isset($dbConfig['type']))
                $className = ($driverNs.strtolower(trim($dbConfig['type'])).'Driver');
            else
                $className = ($driverNs."mysqlDriver");

            if (isset($dbConfig['ConnectionClass']))
            {
                $dcClassName = trim($dbConfig['ConnectionClass']);
                if ($dcClassName != '')
                {
                    $dcObject = new $dcClassName();
                    if (!($dcObject instanceof IDataConnection))
                        throw new SelifaException(DB_INVALID_DATABASE_CONNECTION,'Connection class ['.$dcClassName.'] is specified but does not implements IDataConnection.');
                }
                else
                    $dcObject = new DataConnection();
            }
            else
                $dcObject = new DataConnection();
            $dcObject->Initialize($dbName,$className,$dbConfig,$this->_DisableError);

            if (isset($dbConfig['Extension']))
            {
                $aExtClassName = trim($dbConfig['Extension']);
                if ($aExtClassName != '')
                {
                    $this->_Extensions[$dbName] = new $aExtClassName();
                    $dcObject->SetExtension($this->_Extensions[$dbName]);
                }
                else if (isset($this->_Extensions[$dbName]))
                    $dcObject->SetExtension($this->_Extensions[$dbName]);
            }
            else if (isset($this->_Extensions[$dbName]))
                $dcObject->SetExtension($this->_Extensions[$dbName]);
            return $dcObject;
        }

        /**
         * @param string $dbName
         * @param IDataExtension $extension
         * @return DBI
         */
        public function SetExtension($dbName,$extension)
        {
            $this->_Extensions[$dbName] = $extension;
            return $this;
        }

        /**
         * @param string $dbName
         * @return null|IDataExtension
         */
        public function GetExtension($dbName)
        {
            if (isset($this->_Extensions[$dbName]))
                return $this->_Extensions[$dbName];
            else
                return null;
        }

        /**
         * @param string $dbName
         * @param bool $singleton
         * @return IDataConnection
         * @throws Exception
         */
        public function GetConnection($dbName='',$singleton=true)
        {
            $askedDBName = $this->_DefaultDBName;
            if ($dbName != '')
                $askedDBName = $dbName;
            if ($singleton)
            {
                if (!isset($this->_Database[$askedDBName]))
                    $this->_Database[$askedDBName] = $this->_LoadDriverAndCreateConnectionInterface($askedDBName);
                return $this->_Database[$askedDBName];
            }
            else
            {
                return $this->_LoadDriverAndCreateConnectionInterface($askedDBName);
            }
        }

        /**
         * @return DBI
         */
        public function CloseAllConnections()
        {
            foreach ($this->_Database as $key => $dci)
                $dci->Close();
            return $this;
        }

        /**
         * @param string $message
         * @param int $code
         * @throws Exception
         */
        public function ThrowDatabaseException($message,$code=DB_GENERAL_ERROR)
        {
            if (!$this->_DisableError)
            {
                if ($this->_DisableLastQueryOnError)
                    $msg = 'DB Error: '.$message;
                else
                    $msg = 'DB Error: ' . $message . ($this->LastQuery != '' ? ', Last Query: ' . $this->LastQuery : '');
                throw new Exception($msg,$code);
            }
        }

        /**
         * @param string $dbName
         * @param bool $createNew
         * @return IDataConnection
         * @throws Exception
         */
        public static function Get($dbName='',$createNew=false)
        {
            if (!$createNew)
                return self::$_Instance->GetConnection($dbName,true);
            else
                return self::$_Instance->GetConnection($dbName,false);
        }

        /**
         * @param string $dbName
         * @return IDataConnection
         * @throws Exception
         */
        public static function Create($dbName='')
        {
            $askedDBName = self::$_Instance->_DefaultDBName;
            if ($dbName != '')
                $askedDBName = $dbName;
            return self::$_Instance->GetConnection($askedDBName,false);
        }

        /**
         *
         */
        public static function CloseAll()
        {
            if (self::$_Instance != null)
                self::$_Instance->CloseAllConnections();
        }
    }
}
?>