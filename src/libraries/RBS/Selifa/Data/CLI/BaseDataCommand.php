<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\CLI;
use RBS\Selifa\Composer\Extension\CommandArgument;
use RBS\Selifa\Composer\Extension\CommandDescriptor;
use RBS\Selifa\Composer\Extension\CommandOption;
use RBS\Selifa\Composer\Interfaces\IConsoleIOWrapper;
use RBS\Selifa\Composer\Interfaces\IFrameworkCommand;
use RBS\Selifa\Composer\Extension\CommandWrapper;
use RBS\Selifa\Data\Feature\SchemaObject;
use Exception;
use DateTime;
use RBS\Utility\F;

/**
 * Class BaseDataCommand
 *
 * @package RBS\Selifa\Data\CLI
 */
abstract class BaseDataCommand implements IFrameworkCommand
{
    /**
     * @var string
     */
    private $_CommandName = '';

    /**
     * @var string
     */
    private  $_CommandDesc = '';

    /**
     * @var string
     */
    protected $_DevDir = '';

    /**
     * @var string
     */
    protected $_Tag = '';

    /**
     * @var string
     */
    protected $_OutputType = 'file';

    /**
     * @var null|SchemaObject
     */
    protected $_Schema = null;


    /**
     * BaseDataCommand constructor.
     *
     * @throws Exception
     */
    public function __construct()
    {
        $params = F::GetParametersFromDocComment($this,'command');
        if (isset($params['name']))
            $this->_CommandName = trim($params['name']);
        if (isset($params['desc']))
            $this->_CommandDesc = trim($params['desc']);

        if ($this->_CommandDesc == '')
            $this->_CommandDesc = 'No description.';
    }

    /**
     * @param CommandDescriptor $descriptor
     */
    protected function AddToDescriptor($descriptor) { }

    /**
     * @param IConsoleIOWrapper $io
     * @param string $content
     * @param string $suffix
     */
    public function WriteContentToFile($io,$content,$suffix='')
    {
        $outputFn = $io->getOption('output-file');
        if ($outputFn === null)
            $outputFn = '';
        $outputFn = trim($outputFn);

        if ($outputFn != '')
            $fileName = $outputFn;
        else
        {
            if ($this->_Tag != '')
                $fn = ((new DateTime())->format('YmdHis').'_'.$this->_Tag.'_'.$suffix.'.sql');
            else
                $fn = ((new DateTime())->format('YmdHis').'_'.$suffix.'.sql');
            $fileName = ($this->_DevDir.'files'.DIRECTORY_SEPARATOR.$fn);
        }

        if ($this->_OutputType == 'file')
        {
            $path = pathinfo($fileName,PATHINFO_DIRNAME);
            if (!file_exists($path))
                mkdir($path,0777,true);
            file_put_contents($fileName,$content);
        }
        else if ($this->_OutputType == 'console')
        {
            $io->write($content,true);
        }
    }

    /**
     * @param CommandWrapper $wrapper
     * @param IConsoleIOWrapper $io
     * @throws Exception
     */
    public function Initialized(CommandWrapper $wrapper, IConsoleIOWrapper $io)
    {
        $cfg = $wrapper->GetConfiguration();
        $this->_DevDir = $cfg->DevDir;

        $schemaFile = trim($io->getOption('schema'));
        $tag = strtolower(trim($io->getOption('tag')));
        if ($tag != '')
        {
            $ext = pathinfo($schemaFile,PATHINFO_EXTENSION);
            $bName = pathinfo($schemaFile,PATHINFO_FILENAME);
            $schemaFile = ($bName.'.'.$tag.'.'.$ext);
        }

        $this->_Schema = new SchemaObject($this->_DevDir,$schemaFile,true);
        $this->_Schema->LoadDiffs($tag,true);

        $this->_Tag = $tag;

        $this->_OutputType = strtolower(trim($io->getOption('output')));
        if (!in_array($this->_OutputType,['file','console']))
            throw new Exception('Invalid output type.');
    }

    /**
     * @return CommandDescriptor
     */
    public function GetDescriptor()
    {
        $desc = new CommandDescriptor();
        $desc->Name = $this->_CommandName;
        $desc->Description = $this->_CommandDesc;

        //$desc->AddArgument('action',CommandArgument::REQUIRED,'Action key to be used.');

        $desc->AddOption('tag',CommandOption::VALUE_OPTIONAL,'Schema tag to differentiate between different schema. Default is empty string.','','a');
        $desc->AddOption('schema',CommandOption::VALUE_REQUIRED,'Schema file to be used. Must reside within dev directory.','dbschema.json','s');
        $desc->AddOption('dbname',CommandOption::VALUE_REQUIRED,'Specify database name to be used. This option only available in init action.','default_db','b');
        $desc->AddOption('target',CommandOption::VALUE_REQUIRED,'Specify target database type.','mysql','g');
        $desc->AddOption('table',CommandOption::VALUE_REQUIRED,'Specify a single table to process.',null,'t');
        $desc->AddOption('no-rul',CommandOption::VALUE_NONE,'Skip Row Update Log script on output.');
        $desc->AddOption('output',CommandOption::VALUE_REQUIRED,"Specify output type ('file' or 'console').",'file','o');
        $desc->AddOption('output-file',CommandOption::VALUE_REQUIRED,'Specify alternative output file.',null,'f');

        $this->AddToDescriptor($desc);
        return $desc;
    }
}
?>