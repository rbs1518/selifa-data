<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\CLI;
use RBS\Selifa\Composer\Extension\CommandArgument;
use RBS\Selifa\Composer\Extension\CommandDescriptor;
use RBS\Selifa\Composer\Extension\CommandOption;
use RBS\Selifa\Composer\Interfaces\IConsoleIOWrapper;
use RBS\Selifa\Composer\Interfaces\IFrameworkCommand;
use RBS\Selifa\Composer\Extension\CommandWrapper;
use RBS\Selifa\Data\Feature\SchemaObject;
use Exception;
use DateTime;

/**
 * Class SelifaDataCommand
 *
 * @package RBS\Selifa\Data\CLI
 */
class SelifaDataCommand implements IFrameworkCommand
{
    /**
     * @var string
     */
    private $_DevDir = '';

    /**
     * @var null|SchemaObject
     */
    private $_Schema = null;

    /**
     * @param IConsoleIOWrapper $io
     * @param string $content
     * @param string $suffix
     */
    public function WriteContentToFile($io,$content,$suffix='')
    {
        $outputFn = $io->getOption('output');
        if ($outputFn === null)
            $outputFn = '';
        $outputFn = trim($outputFn);

        if ($outputFn != '')
            $fileName = $outputFn;
        else
        {
            $fn = ((new DateTime())->format('YmdHis').'_'.$suffix.'.sql');
            $fileName = ($this->_DevDir.'files'.DIRECTORY_SEPARATOR.$fn);
        }

        $path = pathinfo($fileName,PATHINFO_DIRNAME);
        if (!file_exists($path))
            mkdir($path,0777,true);
        file_put_contents($fileName,$content);
    }

    /**
     * @param CommandWrapper $wrapper
     * @param IConsoleIOWrapper $io
     * @throws Exception
     */
    public function Initialized(CommandWrapper $wrapper, IConsoleIOWrapper $io)
    {
        $cfg = $wrapper->GetConfiguration();
        $this->_DevDir = $cfg->DevDir;

        $schemaFile = trim($io->getOption('schema'));
        $this->_Schema = new SchemaObject($this->_DevDir,$schemaFile,true);
    }

    /**
     * @return CommandDescriptor
     */
    public function GetDescriptor()
    {
        $desc = new CommandDescriptor();
        $desc->Name = 'selifa-data';
        $desc->Description = 'Data and schema tools for selifa/data library.';

        $desc->AddArgument('action',CommandArgument::REQUIRED,'Action key to be used.');
        //$desc->AddArgument('dbname',CommandArgument::OPTIONAL,'Database name.');

        $desc->AddOption('tag',CommandOption::VALUE_OPTIONAL,'Schema tag to differentiate between different schema. Default is empty string.','','a');
        $desc->AddOption('schema',CommandOption::VALUE_REQUIRED,'Schema file to be used. Must reside within dev directory.','dbschema.json','s');
        $desc->AddOption('dbname',CommandOption::VALUE_REQUIRED,'Specify database name to be used. This option only available in init action.','default_db','b');
        $desc->AddOption('target',CommandOption::VALUE_REQUIRED,'Specify target database type.','mysql','g');
        $desc->AddOption('table',CommandOption::VALUE_REQUIRED,'Specify a single table to process.',null,'t');
        $desc->AddOption('no-rul',CommandOption::VALUE_NONE,'Skip Row Update Log script on output.');
        $desc->AddOption('output',CommandOption::VALUE_REQUIRED,'Specify alternative output file.',null,'o');

        return $desc;
    }

    /**
     * @param IConsoleIOWrapper $io
     * @throws Exception
     */
    public function Execute(IConsoleIOWrapper $io)
    {
        if (!$io->hasArgument('action'))
            throw new Exception('<action> is required.');

        $actKey = strtolower(trim($io->getArgument('action')));
        switch ($actKey)
        {
            case 'init':
                $dbName = trim($io->getOption('dbname'));
                $this->_Schema->SetDatabaseName($dbName);
                $this->_Schema->Save();
                $io->write('Schema file initialized.',true);
                break;
            case 'dbs':
                if (!$io->hasOption('target'))
                    throw new Exception('target database type must be specified.');
                $dbType = strtolower(trim($io->getOption('target')));
                $skipRUL = $io->hasOption('no-rul');

                $sGen = $this->_Schema->CreateScriptGenerator($dbType);

                $tableName = $io->getOption('table');
                if ($tableName !== null)
                {
                    $tableName = trim($tableName);
                    if ($tableName == '')
                        throw new Exception('Empty tablename option.');

                    $content = $sGen->CreateTable($tableName,$skipRUL);
                    $this->WriteContentToFile($io,$content,$tableName);
                }
                else
                {
                    $content = $sGen->CreateFull($skipRUL);
                    $this->WriteContentToFile($io,$content,'schema');
                }

                $io->write('Script generated.',true);
                break;
            case 'itemplate':

                break;
        }

    }
}
?>