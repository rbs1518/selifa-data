<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\CLI;
use RBS\Selifa\Composer\Extension\CommandArgument;
use RBS\Selifa\Composer\Extension\CommandOption;
use RBS\Selifa\Composer\Extension\CommandWrapper;
use RBS\Selifa\Composer\Extension\CommandDescriptor;
use RBS\Selifa\Composer\Interfaces\IConsoleIOWrapper;
use Exception;

/**
 * Class DataSchemaCommand
 *
 * @command name data-schema
 * @command desc Generate schema ddl file for specified dbms based on schema json file.
 *
 * @package RBS\Selifa\Data\CLI
 */
class DataSchemaCommand extends BaseDataCommand
{
    /**
     * @param CommandDescriptor $descriptor
     */
    protected function AddToDescriptor($descriptor)
    {
        $descriptor->AddArgument('diff-flag',CommandArgument::OPTIONAL,
            "Specify which specification to generate ('all','initial','latest','[diffname[-how]]')",
            'all');
        $descriptor->AddOption('trigger-only',CommandOption::VALUE_NONE,'Only generate triggers.');
    }

    /**
     * @param CommandWrapper $wrapper
     * @param IConsoleIOWrapper $io
     * @throws Exception
     */
    public function Initialized(CommandWrapper $wrapper, IConsoleIOWrapper $io)
    {
        parent::Initialized($wrapper, $io);

        $diffType = strtolower(trim($io->getArgument('diff-flag')));
        if ($diffType == 'all')
            $this->_Schema->SetDiffTarget(DATA_SCHEMA_DIFF_ALL,'');
        else if ($diffType == 'initial')
            $this->_Schema->SetDiffTarget(DATA_SCHEMA_DIFF_INITIAL,'');
        else if ($diffType == 'latest')
            $this->_Schema->SetDiffTarget(DATA_SCHEMA_DIFF_LATEST_ONLY,'');
        else
        {
            $dss = explode('-',$diffType,2);
            if (count($dss) == 1)
                $this->_Schema->SetDiffTarget(DATA_SCHEMA_DIFF_SPECIFIC,trim($dss[0]));
            else if (count($dss) == 2)
            {
                $dHow = trim($dss[1]);
                if ($dHow == 'only')
                    $this->_Schema->SetDiffTarget(DATA_SCHEMA_DIFF_SPECIFIC_AGAINST_INITIAL,trim($dss[0]));
                else
                    throw new Exception('Unknown [how] for [diffname] value.');
            }
            else
                throw new Exception('Empty [diffname] value.');
        }
    }

    /**
     * @param IConsoleIOWrapper $io
     * @throws Exception
     */
    public function Execute(IConsoleIOWrapper $io)
    {
        if (!$io->hasOption('target'))
            throw new Exception('target database type must be specified.');
        $dbType = strtolower(trim($io->getOption('target')));
        $skipRUL = $io->getOption('no-rul');
        $triggerOnly = $io->getOption('trigger-only');

        $sGen = $this->_Schema->CreateScriptGenerator($dbType);

        $type = $this->_Schema->GetDiffType();
        if (($type == DATA_SCHEMA_DIFF_ALL) || ($type == DATA_SCHEMA_DIFF_INITIAL))
        {
            $tableName = $io->getOption('table');
            if ($tableName !== null)
            {
                $tableName = trim($tableName);
                if ($tableName == '')
                    throw new Exception('Empty tablename option.');

                $content = $sGen->CreateTable($tableName,$skipRUL,$triggerOnly);
                $this->WriteContentToFile($io,$content,$tableName);
            }
            else
            {
                $content = $sGen->CreateFull($skipRUL,$triggerOnly);
                $this->WriteContentToFile($io,$content,'schema');
            }
        }
        else if ($type == DATA_SCHEMA_DIFF_LATEST_ONLY)
        {

        }
        else if ($type == DATA_SCHEMA_DIFF_SPECIFIC)
        {

        }
        else if ($type == DATA_SCHEMA_DIFF_SPECIFIC_AGAINST_INITIAL)
        {

        }

        $io->write('Script generated.',true);
    }
}
?>