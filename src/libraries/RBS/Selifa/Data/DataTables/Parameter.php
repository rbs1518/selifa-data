<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\DataTables;
use Exception;
use RBS\Selifa\Data\Interfaces\IDataConnection;
use RBS\Selifa\Data\Interfaces\IDataTable;
use RBS\Selifa\Data\SQL\Statement;
use RBS\Selifa\Data\DBI;

/**
 * Class Parameter
 * @package RBS\Selifa\Data\DataTables
 */
class Parameter
{
    /**
     * @var array
     */
    private $_QueryParameters = array();

    /**
     * @var null|IDataConnection
     */
    protected $Driver = null;

    /**
     * @var string
     */
    public $VersionKey = '';

    /**
     * @var string
     */
    public $Echo = '';

    /**
     * @var int
     */
    public $DisplayStart = 0;

    /**
     * @var int
     */
    public $DisplayLength = 0;

    /**
     * @var int
     */
    public $ColumnCount = 0;

    /**
     * @var mixed|null|string
     */
    public $SearchText = '';

    /**
     * @var bool
     */
    public $IsRegexSearch = false;

    /**
     * @var int
     */
    public $SortCount = 0;

    /**
     * @var ColumnInfo[]
     */
    public $Columns = array();

    /**
     * @var SortInfo[]
     */
    public $Sorts = array();

    /**
     * @var null|array
     */
    public $FilterSpec = null;

    /**
     * @var null
     */
    public $ExtendedFilters = null;

    /**
     * @var bool
     */
    public $UseExtendedFilters = false;

    /**
     * @var null|Statement
     */
    public $DataStatement = null;

    /**
     * @var null|Statement
     */
    public $FilteredCountStatement = null;

    /**
     * @var null|Statement
     */
    public $TotalCountStatement = null;

    /**
     * @param array $data
     * @param string $keyName
     * @param mixed|null $default
     * @return mixed|null
     */
    protected function Extract($data, $keyName, $default = null)
    {
        if (isset($data[$keyName]))
            return $data[$keyName];
        return $default;
    }

    /**
     * @return array|mixed
     */
    protected function GetDataFromRequest()
    {
        $useJSON = false;

        $contentType = strtolower(trim($_SERVER['CONTENT_TYPE']));
        if (($contentType == 'text/json') || ($contentType == 'application/json'))
            $useJSON = true;

        if ($useJSON)
            return json_decode(file_get_contents('php://input'),true);
        else
        {
            $rMethod = strtoupper(trim($_SERVER['REQUEST_METHOD']));
            if (($rMethod == 'PUT') || ($rMethod == 'PATCH'))
            {
                $var = file_get_contents('php://input');
                $theData = array();
                parse_str($var,$theData);
                return $theData;
            }
            else
                return $_POST;
        }
    }

    /**
     * @param array $specs
     * @param null|array $filters
     * @param string $logic
     * @return string
     * @throws Exception
     */
    protected function SimpleFilterArrayToSQL($specs,$filters=null,$logic='AND')
    {
        $whereSQL = '';
        if ($filters != null)
        {
            if (count($filters) > 0)
            {
                if ($this->Driver === null)
                    throw new Exception('No database driver loaded.');

                foreach ($filters as $key => $value)
                {
                    if (!isset($specs[$key]))
                        continue;
                    if(strpos(strtolower($key), 'date') !== false && is_array($value))
                        //TODO use dialect object to replace "between" keyword.
                        $whereSQL .= (' '.$logic.' ('. $key .' BETWEEN ("'. $value[0] .' 00:00:00") AND ("'. $value[1] .' 23:59:59"))');
                    else
                    {
                        $spec = $specs[$key];
                        $aValue = $this->Driver->EscapeString($value);
                        if (isset($spec[2]))
                            $aValue = ($spec[2].$aValue);
                        if (isset($spec[3]))
                            $aValue .= $spec[3];
                        $whereSQL .= (' '.$logic.' ('.$spec[0].' '.strtoupper($spec[1])." '".$aValue."')");
                    }
                }
            }
        }
        return $whereSQL;
    }

    /**
     * @param Statement $statement
     */
    protected function InjectParametersIntoStatement(Statement &$statement)
    {
        foreach ($this->_QueryParameters as $value)
            $statement->AddParameter($value);
    }

    /**
     * Parameter constructor.
     *
     * @param null|array $dSource
     * @param string $dbDriverName
     * @param string $verKey
     * @throws Exception
     */
    public function __construct($dSource=null,$dbDriverName='',$verKey='1.10')
    {
        if ($dSource === null)
            $dSource = $this->GetDataFromRequest();
        $this->Driver = DBI::Get($dbDriverName);

        $verKey = $this->Extract($dSource,'dtversion',$verKey);
        if ($verKey == '1.10')
        {
            $this->Echo = $this->Extract($dSource, 'draw', '');

            $columns = $this->Extract($dSource,'columns',array());
            if (!is_array($columns))
                throw new Exception("DataTable parameter 'columns' is not an array.");

            $this->ColumnCount = count($columns);
            $this->DisplayStart = (int)$this->Extract($dSource, 'start', 0);

            $iDisplayLength = $this->Extract($dSource, 'length', '0');
            if ($iDisplayLength == 'all')
                $this->DisplayLength = 0;
            else
                $this->DisplayLength = (int)$iDisplayLength;

            $searchOpt = $this->Extract($dSource,'search',null);
            if ($searchOpt !== null)
            {
                $this->SearchText = $this->Extract($searchOpt,'value','');
                $this->IsRegexSearch = (boolean)$this->Extract($searchOpt, 'regex', false);
            }

            $orders = $this->Extract($dSource,'order',array());
            if (!is_array($orders))
                throw new Exception("DataTable parameter 'order' is not an array.");

            $this->SortCount = count($orders);
            for ($i = 0; $i < $this->ColumnCount; $i++)
                $this->Columns[] = new ColumnInfo($columns,$i,$verKey);

            for ($i = 0; $i < $this->SortCount; $i++)
                $this->Sorts[] = new SortInfo($orders,$i,$verKey,$columns);

            $extFilters = $this->Extract($dSource,'ExtFilters',null);
            if ($extFilters !== null)
            {
                $this->ExtendedFilters = $extFilters;
                $this->UseExtendedFilters = true;
            }
        }
        else
        {
            $this->Echo = $this->Extract($dSource, 'sEcho', '');
            $this->ColumnCount = (int)$this->Extract($dSource, 'iColumns', 0);
            $this->DisplayStart = (int)$this->Extract($dSource, 'iDisplayStart', 0);

            $iDisplayLength = $this->Extract($dSource, 'iDisplayLength', '0');
            if ($iDisplayLength == 'all')
                $this->DisplayLength = 0;
            else
                $this->DisplayLength = (int)$iDisplayLength;

            $this->SearchText = $this->Extract($dSource, 'aSearch', $this->Extract($dSource,'sSearch',''));
            $this->IsRegexSearch = (boolean)$this->Extract($dSource, 'bRegex', false);
            $this->SortCount = (int)$this->Extract($dSource, 'iSortingCols', 0);

            for ($i = 0; $i < $this->ColumnCount; $i++)
                $this->Columns[] = new ColumnInfo($dSource, $i, $verKey);

            for ($i = 0; $i < $this->SortCount; $i++)
                $this->Sorts[] = new SortInfo($dSource, $i, $verKey);

            if (isset($dSource['ExtFilters']))
                $this->ExtendedFilters = $dSource['ExtFilters'];
            else if (isset($dSource['AdvFilters'])) //for backward compatibility with pre-2019 dtx version.
                $this->ExtendedFilters = $dSource['AdvFilters'];
            $this->UseExtendedFilters = ($this->ExtendedFilters !== null);
        }
        $this->VersionKey = $verKey;
    }

    /**
     * @param array $aData
     * @param int $totalCount
     * @param int|null $filteredCount
     * @param string $errorMsg
     * @return array
     */
    public function GenerateResult($aData,$totalCount,$filteredCount=null,$errorMsg='')
    {
        if ($filteredCount === null)
            $filteredCount = $totalCount;
        if ($this->VersionKey == '1.10')
        {
            $result = array(
                'draw' => $this->Echo,
                'recordsTotal' => $totalCount,
                'recordsFiltered' => $filteredCount,
                'data' => $aData
            );
            if ($errorMsg != '')
                $result['error'] = $errorMsg;
            return $result;
        }
        else
        {
            return array(
                'sEcho' => $this->Echo,
                'iTotalRecords' => $totalCount,
                'iTotalDisplayRecords' => $filteredCount,
                'aaData' => $aData,
                'DisplayLength' => $this->DisplayLength,
                'DisplayStart' => $this->DisplayStart
            );
        }
    }

    /**
     * @param Statement|string $dataStatement
     * @return null|Statement
     * @throws Exception
     */
    public function SetDataStatement($dataStatement)
    {
        if ($dataStatement instanceof Statement)
            $this->DataStatement = $dataStatement;
        else if (is_string($dataStatement))
        {
            if ($this->Driver === null)
                throw new Exception('No database driver loaded.');
            $this->DataStatement = new Statement($this->Driver,$dataStatement);

            $aArgs = func_get_args();
            for ($i=1;$i<count($aArgs);$i++)
                $this->DataStatement->AddParameter($aArgs[$i]);
        }
        else
            throw new Exception('Data statement is not an instance of SQLStatement or a string.');
        return $this->DataStatement;
    }

    /**
     * @param Statement|string $fCountStatement
     * @return null|Statement
     * @throws Exception
     */
    public function SetFilteredCountStatement($fCountStatement)
    {
        if ($fCountStatement instanceof SQLStatement)
            $this->FilteredCountStatement = $fCountStatement;
        else if (is_string($fCountStatement))
        {
            if ($this->Driver === null)
                throw new Exception('No database driver loaded.');
            $this->FilteredCountStatement = new Statement($this->Driver,$fCountStatement);

            $aArgs = func_get_args();
            for ($i=1;$i<count($aArgs);$i++)
                $this->FilteredCountStatement->AddParameter($aArgs[$i]);
        }
        else
            throw new Exception('Filtered count statement is not an instance of SQLStatement or a string.');
        return $this->FilteredCountStatement;
    }

    /**
     * @param Statement|string $tCountStatement
     * @return null|Statement
     * @throws Exception
     */
    public function SetTotalCountStatement($tCountStatement)
    {
        if ($tCountStatement instanceof Statement)
            $this->TotalCountStatement = $tCountStatement;
        else if (is_string($tCountStatement))
        {
            if ($this->Driver === null)
                throw new Exception('No database driver loaded.');
            $this->TotalCountStatement = new Statement($this->Driver,$tCountStatement);

            $aArgs = func_get_args();
            for ($i=1;$i<count($aArgs);$i++)
                $this->TotalCountStatement->AddParameter($aArgs[$i]);
        }
        else
            throw new Exception('Data statement is not an instance of SQLStatement or a string.');
        return $this->TotalCountStatement;
    }

    /**
     * @return Parameter
     */
    public function ClearQueryParameters()
    {
        $this->_QueryParameters = array();
        return $this;
    }

    /**
     * @param mixed $value
     * @return Parameter
     */
    public function AddQueryParameter($value)
    {
        $this->_QueryParameters[] = $value;
        return $this;
    }

    /**
     * @param callable $dtCallback
     * @param null|callable $fcCallback
     * @param null|callable $tcCallback
     * @return array
     */
    public function ProcessDefault($dtCallback,$fcCallback=null,$tcCallback=null)
    {
        $aOutput = array();
        try
        {
            if (!is_callable($dtCallback))
                throw new Exception('dtCallback is not a callable object');

            $filters = array();
            $orders = array();
            $ctCols = array();

            foreach ($this->Columns as $ciItem)
            {
                if ($ciItem->IsSearchable && ($ciItem->SearchText != ''))
                {
                    $filters[] = array(
                        'field' => $ciItem->ColumnName,
                        'operator' => 'contains',
                        'value' => $ciItem->SearchText
                    );
                }
            }

            foreach ($this->Sorts as $siItem)
            {
                if ($siItem->IsSortable)
                {
                    $orders[] = array(
                        'col' => $siItem->ColumnName,
                        'dir' => strtoupper($siItem->SortDirection)
                    );
                }
            }

            $data = $dtCallback($filters,$orders,$ctCols,$aOutput);
            if ($data instanceof IDataTable)
                $data = $data->ToArray(true);

            $tCount = 0;
            if (is_callable($tcCallback))
                $tCount = $tcCallback($aOutput);
            if ($tCount < 0)
                $tCount = count($data);

            if ($this->DisplayLength <= 0)
                $fCount = count($data);
            else
            {
                if (is_callable($fcCallback))
                    $fCount = $fcCallback($filters,$aOutput);
                else
                    $fCount = $tCount;
            }

            if (($fCount < 0) && ($tCount > 0))
                $fCount = $tCount;

            if ($this->VersionKey == '1.10')
            {
                $aOutput['draw'] = $this->Echo;
                $aOutput['recordsTotal'] = ($tCount>0?$tCount:$fCount);
                $aOutput['recordsFiltered'] = $fCount;
                $aOutput['data'] = $data;
            }
            else
            {
                $aOutput['sEcho'] = $this->Echo;
                $aOutput['iTotalRecords'] = ($tCount>0?$tCount:$fCount);
                $aOutput['iTotalDisplayRecords'] = $fCount;
                $aOutput['aaData'] = $data;
                $aOutput['DisplayLength'] = $this->DisplayLength;
                $aOutput['DisplayStart'] = $this->DisplayStart;
            }
        }
        catch (Exception $x)
        {
            $aOutput['draw'] = $this->Echo;
            $aOutput['recordsTotal'] = 0;
            $aOutput['recordsFiltered'] = 0;
            $aOutput['data'] = array();
            $aOutput['error'] = $x->getMessage();
        }
        return $aOutput;
    }

    /**
     * @param callable $callback
     * @param null|callable $fcCallback
     * @param null|callable $tcCallback
     * @param array $aOutput
     * @return array|null
     * @throws Exception
     */
    public function ProcessWithAdvancedFilters($callback,$fcCallback=null,$tcCallback=null,&$aOutput=array())
    {
        if ($this->ExtendedFilters === null)
            throw new Exception('Parameter(s) or data source does not contains any extended filter(s).');

        $filters = array();
        $orders = array();
        $ctCols = array();
        foreach ($this->ExtendedFilters as $item)
        {
            $fKey = trim($item['filterKey']);
            if ($item['nullFilter'] != 'true')
            {
                $op = strtolower(trim($item['operator']));
                $filters[] = array(
                    'field' => ($fKey!=''?$fKey:trim($item['targetKey'])),
                    'operator' => ($op!=''?$op:'contains'),
                    'value' => $item['filter']
                );
            }

            if ($fKey != '')
            {
                if ($item['sort'] != '')
                {
                    $orderNo = (int)$item['sortOrder'];
                    if ($orderNo > 0)
                        $orders[$orderNo-1] = array(
                            'col' => $fKey,
                            'dir' => strtoupper(trim($item['sort']))
                        );
                    else
                        array_push($orders,array(
                            'col' => $fKey,
                            'dir' => strtoupper(trim($item['sort']))
                        ));
                }
            }
            else
            {
                if ($item['sort'] != '')
                {
                    $orderNo = (int)$item['sortOrder'];
                    if ($orderNo > 0)
                        $orders[$orderNo-1] = array(
                            'col' => $item['targetKey'],
                            'dir' => strtoupper(trim($item['sort']))
                        );
                    else
                        array_push($orders,array(
                            'col' => $item['targetKey'],
                            'dir' => strtoupper(trim($item['sort']))
                        ));
                }
            }

            if ($item['isCustom'] == 'true')
                $ctCols[$item['key']] = $item['targetKey'];
        }

        if (!is_callable($callback))
            return null;

        $data = $callback($filters,$orders,$ctCols,$aOutput);
        if ($data instanceof IDataTable)
            $data = $data->ToArray(true);

        $tCount = 0;
        if (is_callable($tcCallback))
            $tCount = $tcCallback($aOutput);
        if ($tCount < 0)
            $tCount = count($data);

        if ($this->DisplayLength <= 0)
            $fCount = count($data);
        else
        {
            if (is_callable($fcCallback))
                $fCount = $fcCallback($filters,$aOutput);
            else
                $fCount = $tCount;
        }

        $aOutput['sEcho'] = $this->Echo;
        $aOutput['iTotalRecords'] = ($tCount>0?$tCount:$fCount);
        $aOutput['iTotalDisplayRecords'] = $fCount;
        $aOutput['aaData'] = $data;
        $aOutput['DisplayLength'] = $this->DisplayLength;
        $aOutput['DisplayStart'] = $this->DisplayStart;

        return $aOutput;
    }

    /**
     * @param string $tablePrefix
     * @return array|null
     * @throws Exception
     */
    public function Process($tablePrefix='z')
    {
        if (!$this->UseExtendedFilters)
        {
            return $this->ProcessDefault(function($fs,$os,$ct,&$out) use(&$tablePrefix)
            {
                $dstm = clone $this->DataStatement;
                $this->InjectParametersIntoStatement($dstm);

                $_dialect = $dstm->GetDialect();
                $dstm->WrapInSelect($tablePrefix);

                $sql = '';
                if ($fs != null)
                {
                    if (count($fs) <= 0)
                        $fs = null;
                    else
                        $fs = array(
                            'condition' => 'AND',
                            'rules' => $fs
                        );
                    $converter = new QBConverter($fs,$_dialect);
                    $sql .= (" ".$converter->Convert($tablePrefix,true));
                }

                if (count($os) > 0)
                {
                    $qOrders = array();
                    for ($i=0;$i<count($os);$i++)
                        $qOrders[$os[$i]['col']] = $os[$i]['dir'];
                    $sql = $_dialect->AppendOrder($sql,$qOrders,$tablePrefix);
                }

                $sql = $_dialect->LimitQuery($sql,$this->DisplayStart,$this->DisplayLength);
                $dstm->Append($sql);

                return $dstm->Execute();

            },function($fs,&$out) use(&$tablePrefix)
            {
                $fstm = clone $this->DataStatement;
                $this->InjectParametersIntoStatement($fstm);

                $_dialect = $fstm->GetDialect();
                $fstm->WrapInCountSelect($tablePrefix,'Total');

                $sql = '';
                if ($fs != null)
                {
                    if (count($fs) <= 0)
                        $fs = null;
                    else
                        $fs = array(
                            'condition' => 'AND',
                            'rules' => $fs
                        );
                    $converter = new QBConverter($fs,$_dialect);
                    $sql .= (" ".$converter->Convert($tablePrefix,true));
                }

                $fstm->Append($sql);
                $temp = $fstm->Execute()->FirstRow();

                if ($temp->Total == null)
                {
                    $fcVal = $temp->FirstColumnValue();
                    return (int)$fcVal;
                }
                else
                    return (int)$temp->Total;

            },function(&$out)
            {
                if ($this->TotalCountStatement == null)
                    return -1;

                $tstm = clone $this->TotalCountStatement;
                $this->InjectParametersIntoStatement($tstm);

                $temp = $tstm->Execute()->FirstRow();
                if ($temp->Total == null)
                {
                    $fcVal = $temp->FirstColumnValue();
                    return (int)$fcVal;
                }
                else
                    return (int)$temp->Total;
            });
        }
        else
        {
            //with extended filters
            return $this->ProcessWithAdvancedFilters(function($fs,$os,$ct,&$out) use(&$tablePrefix)
            {
                $dstm = clone $this->DataStatement;
                $this->InjectParametersIntoStatement($dstm);

                $_dialect = $dstm->GetDialect();

                $sql = '';
                if ($fs != null)
                {
                    if (count($fs) <= 0)
                        $fs = null;
                    else
                        $fs = array(
                            'condition' => 'AND',
                            'rules' => $fs
                        );
                    $converter = new QBConverter($fs,$_dialect);
                    $sql .= (" ".$converter->Convert($tablePrefix,true));
                }
                if (count($os) > 0)
                {
                    $qOrders = array();
                    for ($i=0;$i<count($os);$i++)
                        $qOrders[$os[$i]['col']] = $os[$i]['dir'];
                    $sql = $_dialect->AppendOrder($sql,$qOrders);
                }
                $sql = $_dialect->LimitQuery($sql,$this->DisplayStart,$this->DisplayLength);

                $dstm->Append($sql);
                return $dstm->Execute();

            },function($fs,&$out) use(&$tablePrefix)
            {
                if ($this->FilteredCountStatement == null)
                    return -1;

                $fstm = clone $this->FilteredCountStatement;
                $this->InjectParametersIntoStatement($fstm);

                $_dialect = $fstm->GetDialect();

                $sql = '';
                if ($fs != null)
                {
                    if (count($fs) <= 0)
                        $fs = null;
                    else
                        $fs = array(
                            'condition' => 'AND',
                            'rules' => $fs
                        );
                    $converter = new QBConverter($fs,$_dialect);
                    $sql .= (" ".$converter->Convert($tablePrefix,true));
                }

                $fstm->Append($sql);
                $temp = $fstm->Execute()->FirstRow();
                if ($temp->Total == null)
                    throw new Exception('"Total" column in filtered count query is not exists.');
                return (int)$temp->Total;

            },function(&$out)
            {
                if ($this->TotalCountStatement == null)
                    return -1;

                $tstm = clone $this->TotalCountStatement;
                $this->InjectParametersIntoStatement($tstm);

                $temp = $tstm->Execute()->FirstRow();
                if ($temp->Total == null)
                    throw new Exception('"Total" column in total count query is not exists.');
                return (int)$temp->Total;
            });
        }
    }
}
?>