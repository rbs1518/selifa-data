<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\DataTables;

/**
 * Class ColumnInfo
 * @package RBS\Selifa\Data\DataTables
 */
class ColumnInfo
{
    /**
     * @var string
     */
    public $ColumnName = '';

    /**
     * @var string
     */
    public $Name = '';

    /**
     * @var string
     */
    public $SearchText = '';

    /**
     * @var bool
     */
    public $IsRegexSearch = false;

    /**
     * @var bool
     */
    public $IsSearchable = false;

    /**
     * @var bool
     */
    public $IsSortable = false;

    /**
     * @param array $data
     * @param string $keyName
     * @param mixed|null $default
     * @return mixed|null
     */
    protected function Extract($data, $keyName, $default = null)
    {
        if (isset($data[$keyName]))
            return $data[$keyName];
        return $default;
    }

    /**
     * @param array $postData
     * @param string $cIndex
     * @param string $verKey
     */
    public function __construct($postData, $cIndex, $verKey)
    {
        if ($verKey == '1.10')
        {
            $this->ColumnName = trim($this->Extract($postData[$cIndex],'data',''));
            $this->Name = trim($this->Extract($postData[$cIndex],'name',''));

            $this->IsSearchable = (bool)$this->Extract($postData[$cIndex],'searchable',false);
            $this->IsSortable = (bool)$this->Extract($postData[$cIndex],'orderable',false);

            $searchOpt = $this->Extract($postData[$cIndex],'search',array());
            $this->SearchText = trim($this->Extract($searchOpt,'value',''));
            $this->IsRegexSearch = (bool)$this->Extract($searchOpt,'regex',false);
        }
        else
        {
            $this->ColumnName = $this->Extract($postData, 'mDataProp_' . $cIndex, '');
            $this->SearchText = $this->Extract($postData, 'sSearch_' . $cIndex, '');
            $this->IsRegexSearch = (boolean)$this->Extract($postData, 'bRegex_' . $cIndex, false);
            $this->IsSearchable = (boolean)$this->Extract($postData, 'bSearchable_' . $cIndex, false);
            $this->IsSortable = (boolean)$this->Extract($postData, 'bSortable_' . $cIndex, false);
        }
    }
}
?>