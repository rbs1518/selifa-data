<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Transformation;
use RBS\Selifa\Data\Interfaces\IRowTransformation;

/**
 * Class DBStringCSVToArray
 *
 * @package RBS\Selifa\Data\Transformation
 */
class DBStringCSVToArray implements IRowTransformation
{
    /**
     * @var string
     */
    private $_Separator = ',';

    /**
     * DBStringCSVToArray constructor.
     * @param string $separator
     */
    public function __construct($separator=',')
    {
        $this->_Separator = $separator;
    }

    /**
     * @param mixed $value
     * @return array|mixed
     */
    public function ConvertTo($value)
    {
        $sValue = trim($value);
        if ($sValue == '')
            return [];
        else
            return explode($this->_Separator,$sValue);
    }

    /**
     * @param mixed $value
     * @return mixed|string
     */
    public function ConvertFrom($value)
    {
        if (is_array($value))
            return implode($this->_Separator,$value);
        else
            return $value;
    }
}
?>