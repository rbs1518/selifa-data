<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Transformation;
use RBS\Selifa\Data\Interfaces\IRowTransformation;

/**
 * Class DBStringToJSON
 * @package MCU\NPMBU\Transformation
 */
class DBStringToJSON implements IRowTransformation
{
    /**
     * @var null|callable
     */
    private $_PreProcessCallback = null;

    /**
     * @var null|callable
     */
    private $_PostProcessCallback = null;

    /**
     * DBStringToJSON constructor.
     * @param null|callable $preProcessCallback
     * @param null|callable $postProcessCallback
     */
    public function __construct($preProcessCallback=null,$postProcessCallback=null)
    {
        $this->_PreProcessCallback = $preProcessCallback;
        $this->_PostProcessCallback = $postProcessCallback;
    }

    /**
     * @param mixed $value
     * @return array|string
     */
    public function ConvertTo($value)
    {
        $data = json_decode($value,true);
        if ($this->_PostProcessCallback != null)
        {
            $callback = $this->_PostProcessCallback;
            $callback($data);
        }
        return $data;
    }

    /**
     * @param mixed $value
     * @return string
     */
    public function ConvertFrom($value)
    {
        $data = $value;
        if ($this->_PreProcessCallback != null)
        {
            $callback = $this->_PreProcessCallback;
            $callback($data);
        }
        return json_encode($data);
    }
}
?>