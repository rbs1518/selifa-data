<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Interfaces;
use RBS\Selifa\Template\ITGXObjectRenderer;
use JsonSerializable;
use Exception;

/**
 * Interface IDataTable
 *
 * @package RBS\Selifa\Data\Interfaces
 */
interface IDataTable extends ITGXObjectRenderer, JsonSerializable
{
    /**
     * @param IDatabaseDriver $driverObject
     * @param array $dbOpts
     * @param resource|mixed $resultInstance
     */
    public function __construct($driverObject,$dbOpts,$resultInstance);

    /**
     * @param IDataExtension $iExtension
     * @return IDataTable
     */
    public function SetExtension($iExtension);

    /**
     * @param bool $processRow
     * @return array
     * @throws Exception
     */
    public function ToArray($processRow=true);

    /**
     * @return bool|IDataRow
     * @throws Exception
     */
    public function Fetch();

    /**
     * @param callable|null $rowCallback
     * @param bool $toDataRow
     * @throws Exception
     */
    public function Each($rowCallback,$toDataRow=true);

    /**
     * @return IDataRow|bool
     * @throws Exception
     */
    public function FirstRow();

    /**
     * @param string $columnName
     * @return IDataTable
     */
    public function WithRowNumbers($columnName='RowNo');

    /**
     * @return IDataTable
     */
    public function ResetNumbers();

    /**
     * @param array|string $sourceOrKey
     * @param IRowTransformation|null $tObject
     * @return IDataTable
     */
    public function RegisterTransformation($sourceOrKey,$tObject=null);

    /**
     * @param string $key
     * @return IRowTransformation|null
     */
    public function GetTransformation($key);

    /**
     * @return array|null
     */
    public function GetMetaData();

    /**
     * @return string
     */
    public function GetTableName();

    /**
     * @return array
     */
    public function GetColumnInfo();
}
?>