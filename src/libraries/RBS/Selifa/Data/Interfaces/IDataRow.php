<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Interfaces;
use RBS\System\ITransferableObject;
use RBS\Selifa\Template\ITGXObjectRenderer;
use JsonSerializable;
use Exception;

/**
 * Interface IDataRow
 *
 * @package RBS\Selifa\Data\Interfaces
 */
interface IDataRow extends ITransferableObject, ITGXObjectRenderer, JsonSerializable
{
    /**
     * @param IDatabaseDriver $driverObject
     * @param array $dbOpts
     * @param array $rowValues
     * @param array $metaData
     */
    public function __construct($driverObject,$dbOpts,$rowValues,$metaData);

    /**
     * @param IDataExtension $iExtension
     * @return IDataRow
     */
    public function SetExtension($iExtension);

    /**
     * @param IDataTransformCapability $object
     * @return IDataRow
     */
    public function SetTransformCapableObject($object);

    /**
     * @param string $name
     * @return string|float|int|bool|null
     */
    public function __get($name);

    /**
     * @return array
     */
    public function GetColumnNames();

    /**
     * @return string|float|int|bool|null
     */
    public function FirstColumnValue();
}
?>