<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Interfaces;
use RBS\Selifa\Data\SQL\Statement;
use Exception;

/**
 * Interface IDataConnection
 *
 * @package RBS\Selifa\Data\Interfaces
 */
interface IDataConnection
{
    /**
     * @param string $dbId
     * @param string $driverCn
     * @param array $dbConfig
     * @param bool $disableError
     */
    public function Initialize($dbId,$driverCn,$dbConfig,$disableError);

    /**
     * @param IDataExtension $iExtension
     */
    public function SetExtension(IDataExtension $iExtension);

    /**
     * @return null|IDataExtension
     */
    public function GetExtensions();

    /**
     * @return IDatabaseDriver
     */
    public function GetDriver();

    /**
     * @param string $sqlCommand
     * @return IRunnableDataObject
     * @throws Exception
     */
    public function Prepare($sqlCommand);

    /**
     * @param string $tableName
     * @return ITableDataObject
     * @throws Exception
     */
    public function GetTable($tableName);

    /**
     * @param string $procedureName
     * @return IRunnableDataObject
     * @throws Exception
     */
    public function UseProcedure($procedureName);

    /**
     * @param string $input
     * @return string
     */
    public function EscapeString($input);

    /**
     *
     */
    public function BeginTransaction();

    /**
     * @param bool $cancel
     */
    public function EndTransaction($cancel=false);

    /**
     * @param string $sql
     * @param int $type
     * @return Statement
     */
    public function CreateStatement($sql='',$type=DBI_SQL_STATEMENT_GET_DATA);

    /**
     *
     */
    public function Close();
}
?>