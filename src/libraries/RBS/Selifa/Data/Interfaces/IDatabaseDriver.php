<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Interfaces;
use RBS\Selifa\Data\BaseDialect;

/**
 * Interface IDatabaseDriver
 *
 * @package RBS\Selifa\Data
 */
interface IDatabaseDriver
{
    /**
     * @param array $connectionSpec
     */
    public function Initialize($connectionSpec);

    /**
     * @return BaseDialect
     */
    public function GetDialect();

    /**
     * @return bool
     */
    public function IsMultiQuerySupported();

    /**
     * @return string
     */
    public function GetDatabaseName();

    /**
     * @return string
     */
    public function GetDriverType();

    /**
     * @param string $name
     * @param array $parameters
     * @return mixed
     */
    public function StoredProcedureSQL($name,$parameters);

    /**
     * @param resource|mixed $result
     * @return mixed
     */
    public function Fetch($result);

    /**
     * @param resource|mixed $result
     */
    public function FreeResult($result);

    /**
     * @param resource|mixed $result
     * @return int
     */
    public function GetRowCount($result);

    /**
     * @param string $query
     * @return resource|bool
     */
    public function Query($query);

    /**
     * @param string $query
     * @return resource|bool
     */
    public function SingleQuery($query);

    /**
     * @param resource $dbResource
     * @param string $tableName
     * @return array
     */
    public function GetResultMetadata($dbResource,$tableName);

    /**
     * @param string $input
     * @return string
     */
    public function EscapeString($input);

    /**
     *
     */
    public function Close();

    /**
     * @return string|float|int|bool
     */
    public function LastInsertedIdentifier();

    /**
     * @return bool
     */
    public function HasError();

    /**
     * @return string
     */
    public function GetError();

    /**
     * @return mixed|null
     */
    public function GetResult();

    /**
     * @return bool
     */
    public function HasMoreResult();

    /**
     *
     */
    public function NextResult();

    /**
     *
     */
    public function TransactionStart();

    /**
     * @param string $id
     */
    public function TransactionSave($id);

    /**
     * @param string $id
     */
    public function TransactionBackTo($id);

    /**
     * @param string $id
     */
    public function TransactionDelete($id);

    /**
     *
     */
    public function TransactionCommit();

    /**
     *
     */
    public function TransactionRollback();
}
?>