<?php
namespace RBS\Selifa\Data\SQL;
use RBS\Selifa\Data\BaseDialect;
use RBS\Selifa\Data\Interfaces\IDataConnection;
use Exception;
use RBS\Selifa\Data\Interfaces\IDataTable;

/**
 * Class Statement
 *
 * @package RBS\Selifa\Data\SQL
 */
class Statement
{
    /**
     * @var null|IDataConnection
     */
    private $_DB = null;

    /**
     * @var string
     */
    private $_SQL = '';

    /**
     * @var int
     */
    private $_Type = DBI_SQL_STATEMENT_GET_DATA;

    /**
     * @var null|array
     */
    private $_Parameters = null;

    /**
     * @var null|array
     */
    private $_TransformationArray = null;

    /**
     * SQLStatement constructor.
     * @param IDataConnection $db
     * @param string $sql
     * @param int $sType
     */
    public function __construct($db,$sql='',$sType=DBI_SQL_STATEMENT_GET_DATA)
    {
        $this->_DB = $db;
        $this->_SQL = $sql;
        $this->_Type = DBI_SQL_STATEMENT_GET_DATA;
    }

    /**
     * @return string
     */
    public function GetString()
    {
        return $this->_SQL;
    }

    /**
     * @param string $sql
     * @return Statement
     */
    public function Prepend($sql)
    {
        $this->_SQL = ($sql.$this->_SQL);
        return $this;
    }

    /**
     * @param string $sql
     * @return Statement
     */
    public function Append($sql)
    {
        $this->_SQL .= $sql;
        return $this;
    }

    /**
     * @param string $tableAlias
     * @return Statement
     */
    public function WrapInSelect($tableAlias)
    {
        $this->_SQL = $this->_DB->Dialect->WrapInSelect($this->_SQL,$tableAlias);
        return $this;
    }

    /**
     * @param string $tableAlias
     * @param string $colName
     * @return Statement
     */
    public function WrapInCountSelect($tableAlias,$colName='total')
    {
        $this->_SQL = $this->_DB->Dialect->WrapInCountSelect($this->_SQL,$tableAlias,$colName);
        return $this;
    }

    /**
     * @return Statement
     */
    public function Clear()
    {
        $this->_SQL = '';
        return $this;
    }

    /**
     * @param mixed $value
     * @return Statement
     */
    public function AddParameter($value)
    {
        if ($this->_Parameters == null)
            $this->_Parameters = array();
        $this->_Parameters[] = $value;
        return $this;
    }

    /**
     * @return null|bool|IDataTable|IDataTable[]
     * @throws Exception
     */
    public function Execute()
    {
        $functionName = 'GetData';
        if ($this->_Type == DBI_SQL_STATEMENT_RUN)
            $functionName = 'Run';

        $dbo = $this->_DB->Prepare($this->_SQL);
        if ($this->_Parameters != null)
            $result = call_user_func_array(array($dbo,$functionName),$this->_Parameters);
        else
            $result = call_user_func(array($dbo,$functionName));
        if ($this->_TransformationArray !== null)
            $result->RegisterTransformation($this->_TransformationArray);
        return $result;
    }

    /**
     * @return null|BaseDialect
     */
    public function GetDialect()
    {
        return $this->_DB->Dialect;
    }

    /**
     * @param array $tArray
     * @return Statement
     */
    public function RegisterTransformationArray($tArray)
    {
        if ($tArray !== null)
            if (count($tArray) > 0)
                $this->_TransformationArray = $tArray;
        return $this;
    }
}
?>