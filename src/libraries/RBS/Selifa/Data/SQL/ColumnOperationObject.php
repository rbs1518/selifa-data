<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\SQL;
use RBS\Selifa\Data\BaseDialect;

/**
 * Class ColumnOperationObject
 * @package RBS\Selifa\Data\SQL
 */
class ColumnOperationObject extends BaseSQLObject
{
    /**
     * @var ColumnObject[]|BaseSQLObject[]
     */
    protected $Members = array();

    /**
     * @var string
     */
    protected $Operator = SQL_COLUMN_OPERATOR_ADDITION;

    /**
     *
     */
    public function __construct()
    {
        $args = func_get_args();
        $argCount = func_num_args();

        if ($argCount > 2)
        {
            if (is_string($args[0]))
            {
                $this->Operator = $args[0];
                unset($args[0]);
            }

            $this->Members = $args;
        }
        else if ($argCount > 1)
        {
            $this->Members = $args;
        }
    }

    /**
     * @param string $operator
     */
    public function SetOperator($operator)
    {
        $this->Operator = $operator;
    }

    /**
     * @param BaseDialect $dialect
     * @param bool $isAliased
     * @return string
     */
    public function toSQLString(BaseDialect $dialect, $isAliased)
    {
        $stringifiedMembers = array();
        foreach ($this->Members as $member)
        {
            if ($member instanceof ColumnObject)
            {
                $s = '';
                if ($member->Owner instanceof QueryObject)
                    $s = ($dialect->S.$member->Owner->Alias.$dialect->E.'.'.$dialect->S.$member->Name.$dialect->E);
                else if ($member->Owner instanceof TableObject)
                    $s = ($dialect->S.$member->Owner->GetAliasedName().$dialect->E.'.'.$dialect->S.$member->Name.$dialect->E);
                $stringifiedMembers[] = $s;
            }
            else if ($member instanceof BaseSQLObject)
            {
                $s = $member->toSQLString($dialect,false);
                $stringifiedMembers[] = $s;
            }
            else if (is_numeric($member))
            {
                $stringifiedMembers[] = $member;
            }
        }

        $s = '('.implode(' '.$this->Operator.' ',$stringifiedMembers).')';
        if ($this->Alias != '')
            $s .= (' '.$dialect->As.' '.$dialect->S.$this->Alias.$dialect->E);
        return $s;
    }
}
?>