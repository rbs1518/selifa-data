<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\SQL;
use RBS\Selifa\Data\BaseDialect;

/**
 * Class RowOrder
 * @package RBS\Selifa\Data\SQL
 */
class RowOrder extends BaseSQLObject
{
    /**
     * @var RowOrderMember[]
     */
    protected $Members = array();

    /**
     *
     */
    public function __construct()
    {
        $args = func_get_args();
        $numArgs = func_num_args();

        if ($numArgs > 0)
        {
            if ($args[0] instanceof ColumnObject)
            {
                $orderType = SQL_ORDER_ASCENDING;
                if ($numArgs >= 2)
                    $orderType = $args[1];
                $this->Members[] = new RowOrderMember($args[0], $orderType);
            }
            else
            {
                if (is_array($args[0]))
                    $this->Members = $args[0];
                else
                    $this->Members = $args;
            }
        }
    }

    /**
     * @param RowOrderMember $object
     */
    public function AddMember($object)
    {
        $this->Members[] = $object;
    }

    /**
     * @return int
     */
    public function GetMemberCount()
    {
        return count($this->Members);
    }

    /**
     * @param BaseDialect $dialect
     * @param bool $isAliased
     * @return string
     */
    public function toSQLString(BaseDialect $dialect, $isAliased)
    {
        $stringifiedMembers = array();
        foreach ($this->Members as $member)
        {
            if ($member instanceof RowOrderMember)
                $stringifiedMembers[] = ($member->Column->toSQLString($dialect,false) . ' ' . $dialect->{$member->OrderType});
        }
        return implode(', ', $stringifiedMembers);
    }
}
?>