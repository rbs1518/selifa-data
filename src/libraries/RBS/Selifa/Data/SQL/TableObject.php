<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\SQL;
use RBS\Selifa\Data\BaseDialect;

/**
 * Class TableObject
 * @package RBS\Selifa\Data\SQL
 */
class TableObject extends BaseTableObject
{
    /**
     * @var string
     */
    private $_Name = '';

    /**
     * @param string $name
     * @param string $alias
     */
    public function __construct($name, $alias = '')
    {
        $this->_Name = $name;
        $this->Alias = $alias;
    }

    /**
     * @param string $name
     * @param string $alias
     * @return TableObject
     */
    public static function Create($name, $alias = '')
    {
        $newTable = new TableObject($name, $alias);
        return $newTable;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->_Name;
    }

    /**
     * @return string
     */
    public function GetAliasedName()
    {
        if ($this->Alias != '')
        {
            return $this->Alias;
        }
        else
        {
            return $this->_Name;
        }
    }

    /**
     * @param BaseDialect $dialect
     * @param bool $isAliased
     * @return string
     */
    public function toSQLString(BaseDialect $dialect, $isAliased)
    {
        return '';
    }
}
?>