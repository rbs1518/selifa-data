<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Model;
use Exception;
use RBS\Selifa\Data\DBI;
use RBS\Selifa\Data\Interfaces\IDataTable;
use RBS\Selifa\Data\Feature\ModelValidator;
use RBS\Selifa\Data\Interfaces\IStaticGetRows;
use RBS\Selifa\Data\Interfaces\IDataRow;

/**
 * Class StandardModel
 *
 * @package RBS\Selifa\Data
 */
abstract class StandardModel extends AbstractModel implements IStaticGetRows
{
    /**
     * Model constructor.
     *
     * @param int|IDataRow $rowOrRowId
     * @throws Exception
     */
    public function __construct($rowOrRowId)
    {
        $this->SetDatabase('')
             ->SetValidator(new ModelValidator())
             ->UseAuditExtension();
        $this->SetColumnIDKey('ID');
        $this->_Preparing();

        if ($rowOrRowId === null)
        {
            //do nothing
        }
        else if (is_numeric($rowOrRowId))
            $this->SetRow($rowOrRowId);
        else if ($rowOrRowId instanceof IDataRow)
        {
            $this->SetData($rowOrRowId);
            $this->KeyIdentifier = (int)$rowOrRowId->ID;
        }

        $this->OnConstructed($rowOrRowId);
    }

    /**
     * @param int|IDataRow $rowOrRowId
     */
    protected function OnConstructed($rowOrRowId) { }

    /**
     * @return mixed
     */
    protected abstract function _Preparing();

    /**
     * @param array $transformations
     * @return string
     */
    protected abstract function _GetRowSQL(&$transformations);

    /**
     * @param int|string $keyIdentifier
     * @return bool|IDataRow
     * @throws Exception
     */
    protected function GetRowData($keyIdentifier)
    {
        $transformations = [];
        $sql = $this->_GetRowSQL($transformations);
        $data = $this->Driver->Prepare($sql)->GetData($keyIdentifier)->FirstRow();
        if ($data == null)
            throw new Exception($this->_TableName.' data specified by ID '.$keyIdentifier.' does not exists.',$this->_BaseErrorCode + 404);
        return $data;
    }

    /**
     * @return null|IDataTable
     * @throws Exception
     */
    public function GetEmptyRowData()
    {
        $transformations = [];
        $sql = $this->_GetRowSQL($transformations);
        return $this->Driver->Prepare($sql)->GetData('');
    }

    /**
     * @param string $sql
     * @param array $transformations
     */
    protected static function _GetDataSQLAndTransformation(&$sql,&$transformations) { }

    /**
     * @param array $filters
     * @param array $orders
     * @param int $offset
     * @param int $limit
     * @return mixed
     * @throws Exception
     */
    public static function GetRows($filters,$orders,$offset,$limit)
    {
        $contentSql = '';
        $transformations = [];

        static::_GetDataSQLAndTransformation($contentSql,$transformations);

        $sql = "SELECT zz.* FROM (".$contentSql.") AS zz"
            .self::SimpleFilterArrayToSQL($filters,'zz')
            .self::SimpleOrderArrayToSQL($orders,'zz');

        $d = DBI::Get()->Dialect;
        if ($limit > 0)
            $sql = $d->LimitQuery($sql,$offset,$limit);
        else if ($limit == -1)
            $sql = $d->LimitQuery($sql,0,0);

        $data = DBI::Get()
            ->Prepare($sql)
            ->GetData()
            ->RegisterTransformation($transformations);
        return $data;
    }
}
?>