<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Model;
use RBS\Selifa\Data\Interfaces\IDataConnection;
use RBS\Selifa\Data\Interfaces\IDataObject;
use RBS\Selifa\Data\Interfaces\IDataRow;
use RBS\Selifa\Data\Interfaces\IDataTable;
use RBS\Selifa\Data\Interfaces\IDataValidator;
use RBS\Selifa\Data\Interfaces\IAuditedDataExtension;
use RBS\Selifa\Data\BaseDialect;
use RBS\Selifa\Data\DBI;
use RBS\System\ITransferableObject;
use Exception;
use SelifaException;
use JsonSerializable;

/**
 * Class AbstractModel
 *
 * @package RBS\Selifa\Data
 */
abstract class AbstractModel implements ITransferableObject, JsonSerializable
{
    /**
     * @var int
     */
    protected $_BaseErrorCode = 100000;

    /**
     * @var string
     */
    protected $_TableName = '';

    /**
     * @var string
     */
    protected $_SoftDeleteKey = '';

    /**
     * @var string
     */
    protected $_IDColumnKey = '';

    /**
     * @var null|IDataConnection
     */
    protected $Driver = null;

    /**
     * @var null|IDataValidator
     */
    protected $Validator = null;

    /**
     * @var null|BaseDialect
     */
    protected $Dialect = null;

    /**
     * @var null|IDataRow
     */
    protected $RowData = null;

    /**
     * @var null|string|int
     */
    protected $KeyIdentifier = null;

    /**
     * @var null|string|int
     */
    protected $CurrentUserID = null;

    /**
     * @var null|IDataObject
     */
    protected $Primary = null;

    /**
     * @var array|AbstractModel[]
     */
    protected $LinkedItemCache = array();

    /**
     * @param string|int $keyIdentifier
     * @return null|IDataRow
     */
    protected function GetRowData($keyIdentifier)
    {
        return null;
    }

    /**
     * @param string $name
     * @return mixed|AbstractModel|null
     */
    protected function GetLinkedItem($name)
    {
        return null;
    }

    #region Functional Constructor
    /**
     * @param string $dbName
     * @return static
     * @throws Exception
     */
    protected function SetDatabase($dbName)
    {
        $this->Driver = DBI::Instance()->GetConnection($dbName);
        $this->Dialect = $this->Driver->Dialect;
        return $this;
    }

    /**
     * @return static
     */
    protected function UseAuditExtension()
    {
        $dbExt = $this->Driver->GetExtensions();
        if ($dbExt instanceof IAuditedDataExtension)
        {
            $this->CurrentUserID = $dbExt->GetUserID();
            $this->_SoftDeleteKey = $dbExt->GetSoftDeleteColumnKey();
        }
        return $this;
    }

    /**
     * @param string $tableName
     * @return static
     * @throws Exception
     */
    protected function SetTable($tableName)
    {
        $this->_TableName = $tableName;
        if ($tableName != '')
            $this->Primary = $this->Driver->GetTable($tableName);
        return $this;
    }

    /**
     * @param string|int $rowId
     * @return static
     */
    protected function SetRow($rowId)
    {
        if (is_numeric($rowId))
            $this->KeyIdentifier = (int)$rowId;
        else
            $this->KeyIdentifier = $rowId;
        if ($rowId != null)
            $this->RowData = $this->GetRowData($rowId);
        return $this;
    }

    /**
     * @param IDataRow|null $rowData
     * @return static
     */
    protected function SetData($rowData)
    {
        $this->RowData = $rowData;
        return $this;
    }

    /**
     * @param int $cUserId
     * @return static
     */
    protected function SetUserID($cUserId)
    {
        if ($cUserId != null)
            $this->CurrentUserID = $cUserId;
        return $this;
    }

    /**
     * @param int $code
     * @return static
     */
    protected function SetBaseErrorCode($code)
    {
        $this->_BaseErrorCode = $code;
        return $this;
    }

    /**
     * @param string $key
     * @return static
     */
    protected function SetColumnIDKey($key)
    {
        $this->_IDColumnKey = $key;
        return $this;
    }

    /**
     * @param string $columnId
     * @param string $softDelete
     * @return static
     */
    protected function SetKeys($columnId,$softDelete)
    {
        $this->_IDColumnKey = $columnId;
        $this->_SoftDeleteKey = $softDelete;
        return $this;
    }
    #endregion

    /**
     * @param int $id
     * @param null|IDataRow $data
     * @param array|null $cols
     * @param string $tableName
     * @param string $idKey
     * @throws Exception
     */
    protected function InitDefaultRowData($id,$data,$cols=null,$tableName='',$idKey='ID')
    {
        if ($data === null)
        {
            $d = $this->Dialect;
            if ($cols === null)
                $cols = array('*');
            if (count($cols) <= 0)
                return;

            if ($tableName == '')
                $tableName = $this->_TableName;

            $sdk = trim($this->_SoftDeleteKey);
            $sql = "SELECT ";
            if (trim($cols[0]) == '*')
                $sql .= "*";
            else
                $sql .= ($d->S.implode($d->S.','.$d->E,$cols).$d->E);
            $sql .= " FROM ".$d->S.$tableName.$d->E." WHERE ";
            if ($sdk != '')
                $sql .= ("(".$d->S.$this->_SoftDeleteKey.$d->E.' = '.$d->False.") AND ");
            $sql .= ("(".$d->S.$this->_IDColumnKey.$d->E.' = @1)');
            $sql = $d->LimitQuery($sql,0,1);
            $this->RowData = $this->Driver->Prepare($sql)->GetData($id)->FirstRow();
            if ($this->RowData == null)
                throw new SelifaException($this->_BaseErrorCode + 404,'',['id' => $id]);
        }
        else
            $this->RowData = $data;
    }

    public function __get($name)
    {
        if (!isset($this->LinkedItemCache[$name]))
        {
            $itemObject = $this->GetLinkedItem($name);
            if ($itemObject == null)
                return null;
            $this->LinkedItemCache[$name] = $itemObject;
        }
        return $this->LinkedItemCache[$name];
    }

    /**
     * @return string|int
     */
    public function GetID()
    {
        return $this->KeyIdentifier;
    }

    /**
     * @return null|IDataRow
     */
    public function GetData()
    {
        return $this->RowData;
    }

    /**
     * @return string
     */
    public function GetTableName()
    {
        return $this->_TableName;
    }

    /**
     * @param IDataValidator $validator
     * @return static
     */
    public function SetValidator(IDataValidator $validator)
    {
        $this->Validator = $validator;
        return $this;
    }

    /**
     * @param string $sqlCommand
     * @return IDataObject
     * @throws Exception
     */
    protected function Prepare($sqlCommand)
    {
        return $this->Driver->Prepare($sqlCommand);
    }

    /**
     * @param string $tableName
     * @return IDataObject
     * @throws Exception
     */
    protected function GetTable($tableName)
    {
        return $this->Driver->GetTable($tableName);
    }

    /**
     * @param string $storedProcedureName
     * @return IDataObject
     * @throws Exception
     */
    protected function UseProcedure($storedProcedureName)
    {
        return $this->Driver->UseProcedure($storedProcedureName);
    }

    #region ITransferableObject
    /**
     * @return array
     */
    public function GetTransferableMembers()
    {
        if ($this->RowData != null)
            return $this->RowData->GetTransferableMembers();
        else
            return array();
    }

    /**
     * @param array|ITransferableObject $tObjectOrArray
     */
    public function To(&$tObjectOrArray)
    {
        if ($this->RowData != null)
            $this->RowData->To($tObjectOrArray);
    }

    /**
     * @return array
     */
    public function ToArray()
    {
        if ($this->RowData != null)
            return $this->RowData->ToArray();
        else
            return array();
    }

    /**
     * @param array|ITransferableObject $tObjectOrArray
     */
    public function From($tObjectOrArray)
    {
        if ($this->RowData != null)
            $this->RowData->From($tObjectOrArray);
    }
    #endregion

    #region JsonSerializable
    /**
     * @return null|IDataRow
     */
    function jsonSerialize()
    {
        if ($this->RowData != null)
            return $this->RowData;
        else
            return null;
    }
    #endregion

    /**
     * @return null|IDataRow
     * @throws Exception
     */
    protected function GetRowForUpdate()
    {
        $id = $this->GetID();
        $uRow = $this->Primary->GetRow($id);
        if ($uRow == null)
            throw new SelifaException($this->_BaseErrorCode + 404,'',['id' => $id]);
        return $uRow;
    }

    /**
     * @param $type
     * @return array|null
     */
    public function GetValidationRules($type)
    {
        return null;
    }

    /**
     * @param array|ITransferableObject $dataToInsert
     * @return IDataRow
     * @throws SelifaException
     */
    public function InsertFields($dataToInsert)
    {
        if ($this->Validator != null)
        {
            $rules = $this->GetValidationRules(MODEL_VALIDATION_TYPE_INSERT);
            if ($rules !== null)
                $dataToInsert = $this->Validator->ValidateData($dataToInsert,$rules);
        }

        $nRow = $this->Primary->NewRow();
        $nRow->From($dataToInsert);
        $b = $nRow->Save();
        if (!$b)
            throw new SelifaException($this->_BaseErrorCode + 501,'');

        $this->KeyIdentifier = $nRow->{$this->_IDColumnKey};
        $this->RowData = $nRow;
        return $nRow;
    }

    /**
     * @param array|ITransferableObject $dataToUpdate
     * @return IDataRow
     * @throws Exception
     */
    public function UpdateFields($dataToUpdate)
    {
        $uRow = $this->Primary->GetRow($this->KeyIdentifier);
        if ($this->Validator != null)
        {
            $rules = $this->GetValidationRules(MODEL_VALIDATION_TYPE_UPDATE);
            if ($rules !== null)
                $dataToUpdate = $this->Validator->ValidateData($dataToUpdate,$rules);
        }

        $uRow->From($dataToUpdate);
        $b = $uRow->Save();
        if (!$b)
            throw new SelifaException($this->_BaseErrorCode + 502,'');
        return $uRow;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function Delete()
    {
        $uRow = $this->Primary->GetRow($this->KeyIdentifier);
        $b = $uRow->Delete();
        if (!$b)
            throw new SelifaException($this->_BaseErrorCode + 503,'');
        return true;
    }

    /**
     * @return null|IDataTable
     */
    public function GetEmptyRowData()
    {
        return null;
    }

    #region Statics
    /**
     * @return IDataConnection
     * @throws Exception
     */
    protected static function InternalGetDriver()
    {
        return DBI::Instance()->GetConnection('');
    }

    /**
     * @param array $filters
     * @param string $tPrefix
     * @return string
     * @throws Exception
     */
    protected static function SimpleFilterArrayToSQL($filters,$tPrefix='')
    {
        $db = self::InternalGetDriver();
        $d = $db->Dialect;

        $aprfx = ($tPrefix != ''?$tPrefix.'.':'');

        $pairs = [];
        foreach ($filters as $key => $value)
        {
            $item =  $aprfx.($d->S.$key.$d->E);
            if (is_string($value))
                $item .= (" LIKE '%".$db->EscapeString($value)."%'");
            else if (is_bool($value))
                $item .= (" = ".($value?$d->True:$d->False));
            else if (is_array($value))
            {
                $s = '';
                foreach ($value as $item)
                {
                    if (is_string($item))
                        $s .= (",'" . $db->EscapeString($item) . "'");
                    else
                        $s .= (',' . $db->EscapeString($item));
                }
                $s[0] = '(';
                $item .=  (" IN ".$s.')');
            }
            else
                $item .= (" = ".$value);
            $pairs[] = ("(".$item.")");
        }

        if (count($pairs) > 0)
            return " WHERE ".implode(' AND ',$pairs);
        else
            return "";
    }

    /**
     * @param array $orders
     * @param string $tPrefix
     * @return string
     * @throws Exception
     */
    protected static function SimpleOrderArrayToSQL($orders,$tPrefix='')
    {
        $db = self::InternalGetDriver();
        $d = $db->Dialect;

        $aprfx = ($tPrefix != ''?$tPrefix.'.':'');

        $pairs = [];
        foreach ($orders as $index => $order)
        {
            if (is_string($index))
                $item = ($aprfx.$d->S.$index.$d->E.' '.$order);
            else
                $item = ($aprfx.$d->S.$order.$d->E);
            $pairs[] = $item;
        }

        if (count($pairs) > 0)
            return " ORDER BY ".implode(', ',$pairs);
        else
            return "";
    }

    /**
     * @param int $id
     * @return static
     * @throws Exception
     */
    public static function ByID($id)
    {
        $class = get_called_class();
        return new $class($id);
    }

    /**
     * @return static
     * @throws Exception
     */
    public static function CreateEmpty()
    {
        $class = get_called_class();
        return new $class(null);
    }

    /**
     * @param array|ITransferableObject $data
     * @return static
     * @throws Exception
     */
    public static function Create($data)
    {
        $class = get_called_class();
        $object = new $class(null);
        $object->InsertFields($data);
        return $object;
    }
    #endregion
}
?>