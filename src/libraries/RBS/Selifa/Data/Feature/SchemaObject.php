<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Feature;
use RBS\Selifa\Data\Interfaces\IDatabaseScriptGenerator;
use Exception;

define('DATA_SCHEMA_DIFF_INITIAL',1);
define('DATA_SCHEMA_DIFF_ALL',2);
define('DATA_SCHEMA_DIFF_LATEST_ONLY',3);
define('DATA_SCHEMA_DIFF_SPECIFIC',4);
define('DATA_SCHEMA_DIFF_SPECIFIC_AGAINST_INITIAL',5);

/**
 * Class SchemaObject
 * @package RBS\Selifa\Data\Feature
 */
class SchemaObject
{
    /**
     * @var string
     */
    private $_DevDir = '';

    /**
     * @var string
     */
    private $_FileName = '';

    /**
     * @var null|array
     */
    private $_Content = null;

    /**
     * @var array
     */
    private $_Scripts = [];

    /**
     * @var int
     */
    private $_DiffType = DATA_SCHEMA_DIFF_ALL;

    /**
     * @var string
     */
    private $_DiffTarget = '';

    /**
     * @var array
     */
    private $_Diffs = [];

    /**
     * @return array
     */
    protected function CreateNewSchema()
    {
        return [
            'Name' => '<replace_with_your_app_database_name>',
            'User' => '<replace_with_your_app_database_user>',
            'Password' => '<replace_with_your_app_database_password>',
            'RowUpdateLog' => true,
            'Defaults' => [
                'IDType' => 'INT',
                'IDLength' => 10,
                'IDAutoIncrement' => true,
                'ChangeMeta' => true,
                'SoftDelete' => true,
                'SoftActive' => true,
                'SoftActiveIndexed' => true,
                'Engine' => 'InnoDB',
                'Charset' => 'utf8',
                'InsertLogTrigger' => true,
                'UpdateLogTrigger' => true,
                'DeleteLogTrigger' => true
            ],
            'Tables' => [],
            'PreScript' => [],
            'PostScript' => []
        ];
    }

    /**
     * SchemaObject constructor.
     *
     * @param string $devDir
     * @param string $fileName
     * @param bool $createNewIfNotExists
     * @throws Exception
     */
    public function __construct($devDir,$fileName,$createNewIfNotExists=false)
    {
        $this->_DevDir = $devDir;
        $this->_FileName = ($devDir.$fileName);
        if (file_exists($this->_FileName))
        {
            $sContent = file_get_contents($this->_FileName);
            $this->_Content = json_decode($sContent,true);
        }
        else if ($createNewIfNotExists)
            $this->_Content = $this->CreateNewSchema();
        else
            throw new Exception('Schema file ['.$this->_FileName.'] does not exists.');

        if (isset($this->_Content['Scripts']))
        {
            foreach ($this->_Content['Scripts'] as $key => $items)
            {
                foreach ($items as $item)
                {
                    $fPath = ($this->_DevDir.$item);
                    if (!file_exists($fPath))
                        throw new Exception('Script file ['.$fPath.'] does not exists.');
                    $content = file_get_contents($fPath);

                    if (!isset($this->_Scripts[$key]))
                        $this->_Scripts[$key] = [];
                    $this->_Scripts[$key][] = $content;
                }
            }
        }

        //TODO need to handle scripts on diff too
    }

    /**
     *
     */
    public function Save()
    {
        $tContent = json_encode($this->_Content,JSON_PRETTY_PRINT);
        file_put_contents($this->_FileName,$tContent);
    }

    /**
     * @param array $init
     * @param array $diff
     * @return array
     */
    public function MergeDiffSpec($init,$diff)
    {
        $final = $init;
        if (isset($diff['Tables']))
        {
            $iTabs = [];
            foreach ($final['Tables'] as $its)
            {
                $tableName = strtoupper(trim($its['Name']));
                $iTabs[$tableName] = $its;
            }

            $newTables = [];
            foreach ($diff['Tables'] as $dts)
            {
                $diffTn = strtoupper(trim($dts['Name']));
                if (!isset($iTabs[$diffTn]))
                {
                    $newTables[] = $dts;
                }
                else
                {
                    /*$oTable = $iTabs[$diffTn];
                    $iCols = [];
                    foreach ($oTable['Columns'] as $ics)
                    {
                        $colName = trim($ics['Name']);
                        $iCols[$colName] = $ics;
                    }

                    if (isset($dts['Columns']))
                    {
                        $oTable['Columns'] = [];
                        $newColumns = [];
                        foreach ($dts['Columns'] as $dcs)
                        {
                            $dColName = trim($dcs['Name']);
                            if (!isset($iCols[$dColName]))
                            {
                                $newColumns[] = $dcs;
                            }
                            else
                            {
                                $oTable['Columns'][] =
                            }
                        }
                    }
                    $final['Tables'][] = $oTable;*/
                }
            }

            foreach ($newTables as $nTable)
                $final['Tables'][] = $nTable;
        }
        return $final;
    }

    /**
     * @return array|null
     */
    public function GetContent()
    {
        if ($this->_DiffType == DATA_SCHEMA_DIFF_INITIAL)
            return $this->_Content;
        else if ($this->_DiffType == DATA_SCHEMA_DIFF_ALL)
        {
            $final = $this->_Content;
            foreach ($this->_Diffs as $key => $diffSpec)
                $final = $this->MergeDiffSpec($final,$diffSpec);
            return $final;
        }
        else if ($this->_DiffType == DATA_SCHEMA_DIFF_LATEST_ONLY)
        {
            $final = $this->_Content;
            foreach ($this->_Diffs as $key => $diffSpec)
                $final = $this->MergeDiffSpec($final,$diffSpec);
            return $final;
        }
        else if ($this->_DiffType == DATA_SCHEMA_DIFF_SPECIFIC)
        {

        }

        return $this->_Content;
    }

    /**
     * @param string $key
     * @return string[]
     */
    public function GetScripts($key)
    {
        if (isset($this->_Scripts[$key]))
            return $this->_Scripts[$key];
        else
            return [];
    }

    /**
     * @param string $dbName
     */
    public function SetDatabaseName($dbName)
    {
        $dbUser = ($dbName.'_user');
        $dbPass = hash('fnv1a64',(time().$dbUser));
        $this->_Content['Name'] = $dbName;
        $this->_Content['User'] = $dbUser;
        $this->_Content['Password'] = $dbPass;
    }

    /**
     * @param string $type
     * @return IDatabaseScriptGenerator
     * @throws Exception
     */
    public function CreateScriptGenerator($type)
    {
        $ns = "\\RBS\\Selifa\\Data\\Driver\\";
        $className = ($ns.$type."ScriptGenerator");
        $obj = new $className();
        if (!($obj instanceof IDatabaseScriptGenerator))
            throw new Exception($className.' does not inherit from IDatabaseScriptGenerator.');
        $obj->Initialize($this);
        return $obj;
    }

    /**
     * @param string $tag
     * @param bool $skipIfNotExists
     * @throws Exception
     */
    public function LoadDiffs($tag,$skipIfNotExists=true)
    {
        $dPath = ($this->_DevDir.'schemadiffs');
        if ($tag != '')
            $dPath .= ('.'.$tag);

        if (!file_exists($dPath))
        {
            if (!$skipIfNotExists)
                throw new Exception('Diffs directory ['.$dPath.'] does not exists.');
            else
                return;
        }

        $files = scandir($dPath,SCANDIR_SORT_ASCENDING);
        if ($files === false)
            throw new Exception('Failed to scan diffs directory.');

        foreach ($files as $file)
        {
            if (($file == '.') || ($file == '..'))
                continue;

            $fExt = strtolower(trim(pathinfo($file,PATHINFO_EXTENSION)));
            if ($fExt != 'json')
                continue;

            $fpName = ($dPath.DIRECTORY_SEPARATOR.$file);
            $keyName = strtolower(trim(pathinfo($file,PATHINFO_FILENAME)));

            $jsonContent = file_get_contents($fpName);
            $this->_Diffs[$keyName] = json_decode($jsonContent,true);
        }

        //TODO need to handle scripts on diff too
    }

    /**
     * @param int $type
     * @param string $target
     */
    public function SetDiffTarget($type,$target)
    {
        $this->_DiffType = $type;
        $this->_DiffTarget = $target;
    }

    /**
     * @return int
     */
    public function GetDiffType()
    {
        return $this->_DiffType;
    }

    /**
     * @return string
     */
    public function GetDiffTarget()
    {
        return $this->_DiffTarget;
    }

    /**
     * @return array
     */
    public function GetDiffSpecs()
    {
        return $this->_Diffs;
    }
}
?>