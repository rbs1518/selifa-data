<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Feature;

/**
 * Trait DatabaseSchemaPreProcessTrait
 *
 * @package RBS\Selifa\Data\Feature
 */
trait DatabaseSchemaPreProcessTrait
{
    /**
     * @var null|SchemaObject
     */
    protected $_SchemaObject = null;

    /**
     * @var string
     */
    protected $_DBName = '';

    /**
     * @var string
     */
    protected $_DBUserName = '';

    /**
     * @var string
     */
    protected $_DBPassword = '';

    /**
     * @var array
     */
    protected $_Tables = [];

    /**
     * @var array
     */
    protected $_TableNames = [];

    /**
     * @var string
     */
    protected $_RULTableName = '_RowUpdateLog';

    /**
     * @var string
     */
    protected $_ActiveFlagName = 'SFlag';

    /**
     * @var string
     */
    protected $_DeleteFlagName = 'DFlag';

    /**
     * @var string[]
     */
    protected $_PreScripts = [];

    /**
     * @var string[]
     */
    protected $_PostScripts = [];

    /**
     * @var string[]
     */
    protected $_NumericTypes = ['INT','INTEGER','TINYINT','SMALLINT','MEDIUMINT','BIGINT','FLOAT','DOUBLE','DECIMAL','NUMERIC','BIT'];

    /**
     * @param SchemaObject $schema
     */
    protected function SetSchemaObject(SchemaObject $schema)
    {
        $this->_SchemaObject = $schema;
    }

    /**
     *
     */
    protected function ParseSchemaSpec()
    {
        $spec = $this->_SchemaObject->GetContent();

        if (isset($spec['Name']))
            $this->_DBName = trim($spec['Name']);
        if (isset($spec['User']))
            $this->_DBUserName = trim($spec['User']);
        if (isset($spec['Password']))
            $this->_DBPassword = trim($spec['Password']);

        $createRUL = false;
        if (isset($spec['RowUpdateLog']))
            $createRUL = (bool)$spec['RowUpdateLog'];

        $tblDefs = [];
        if (isset($spec['Defaults']))
            $tblDefs = $spec['Defaults'];

        $iTables = [];
        if (isset($spec['Tables']))
            $iTables = $spec['Tables'];

        if (isset($spec['ActiveFlagName']))
            $this->_ActiveFlagName = trim($spec['ActiveFlagName']);
        if (isset($spec['DeleteFlagName']))
            $this->_DeleteFlagName = trim($spec['DeleteFlagName']);

        if ($createRUL)
        {
            $rulTable = $this->CreateRULSpec();
            $tableName = $rulTable['Name'];
            $this->_Tables[$tableName] = $rulTable;
            $this->_TableNames[] = $tableName;
        }

        foreach ($iTables as $tbl)
        {
            $ftbls = $tblDefs;
            $ftbls = array_replace_recursive($ftbls,$tbl);

            $tableName = trim($ftbls['Name']);
            $this->_Tables[$tableName] = $ftbls;
            $this->_TableNames[] = $tableName;
        }

        if (isset($spec['PreScript']))
            $this->_PreScripts = $spec['PreScript'];
        if (isset($spec['PostScript']))
            $this->_PostScripts = $spec['PostScript'];
    }

    /**
     * @return array
     */
    protected function CreateRULSpec()
    {
        return [
            'Name' => $this->_RULTableName,
            'IDType' => 'BIGINT',
            'IDLength' => 20,
            'IDAutoIncrement' => true,
            'Engine' => 'InnoDB',
            'Charset' => 'utf8',
            'Columns' => [
                [
                    'Name' => 'LogTime',
                    'Type' => 'TIMESTAMP',
                    'Nullable' => false,
                    'Default' => 'CURRENT_TIMESTAMP'
                ],
                [
                    'Name' => 'DBUser',
                    'Type' => 'VARCHAR',
                    'Length' => 64,
                    'Nullable' => false,
                    'Indexed' => true
                ],
                [
                    'Name' => 'UserID',
                    'Type' => 'INTEGER',
                    'Nullable' => false,
                    'Sign' => 'UNSIGNED',
                    'Default' => 0,
                    'Indexed' => true
                ],
                [
                    'Name' => 'TableName',
                    'Type' => 'VARCHAR',
                    'Length' => 64,
                    'Nullable' => false,
                    'Indexed' => true
                ],
                [
                    'Name' => 'RowID',
                    'Type' => 'BIGINT',
                    'Length' => 20,
                    'Nullable' => false,
                    'Sign' => 'UNSIGNED',
                    'Default' => 0,
                    'Indexed' => true
                ],
                [
                    'Name' => 'EventType',
                    'Type' => 'VARCHAR',
                    'Length' => 16,
                    'Nullable' => false,
                    'Indexed' => true
                ],
                [
                    'Name' => 'Data',
                    'Type' => 'LONGTEXT',
                    'Nullable' => false
                ],
            ],
            'Indexes' => [
                [
                    'Name' => 'SourceRow',
                    'Columns' => ['TableName','RowID']
                ]
            ],
            'SkipModelFile' => true,
            'SkipModelController' => true
        ];
    }

    /**
     * @param string $deleteFlag
     * @param string $activeFlag
     */
    public function SetSoftColumnNames($deleteFlag,$activeFlag)
    {
        $this->_DeleteFlagName = $deleteFlag;
        $this->_ActiveFlagName = $activeFlag;
    }

    /**
     * @param string $name
     */
    public function SetRULTableName($name)
    {
        $this->_RULTableName = $name;
    }

    //TODO create function to get diff content for generator.
    /*
     * array if two key, 'init', and 'target', init contains asll spec until just before the target.
     * while target contains the diff for the target itself.
     * if SPECIFIC_AGAINST_INIT, then init contains original/init spec, and target contains the diff for the target itself.
     */

    ///TODO create function to calculate the diffs between init and target, will return any new columns or new tables
}
?>