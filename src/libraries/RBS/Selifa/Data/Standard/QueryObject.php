<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Standard;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use RBS\Selifa\Data\Interfaces\IRunnableDataObject;
use RBS\Selifa\Data\Interfaces\IDataTable;
use RBS\Selifa\Data\DBI;
use Exception;

/**
 * Class QueryObject
 *
 * @package RBS\Selifa\Data\Standard
 */
class QueryObject extends BaseDataObject implements IRunnableDataObject
{
    /**
     * @var array
     */
    private $_RSCMDS = array('SELECT','SHOW','DESC','DESCRIBE');

    /**
     * @var bool
     */
    private $_IsResultSet = false;

    /**
     * @var string
     */
    private $_ParameterizedQueryPattern = '#@([0-9]+)#';

    /**
     * @var array
     */
    private $_QueryParameters = array();

    /**
     * @param IDatabaseDriver $driverObject
     * @param string $strCommand
     * @param array $dbOpts
     * @throws Exception
     */
    public function __construct($driverObject,$dbOpts,$strCommand)
    {
        $this->_Driver = $driverObject;
        $this->_DBOptions = $dbOpts;

        if ($strCommand == '')
            DBI::Instance()->ThrowDatabaseException('SQL command is empty');
        $iPos = strpos($strCommand,' ',0);
        $sCheck = ($iPos < 0?strtoupper($strCommand):strtoupper(substr($strCommand,0,$iPos)));
        $this->_IsResultSet = in_array($sCheck,$this->_RSCMDS);
        $this->_SQL = $strCommand;
    }

    /**
     * @param string $key
     * @return string
     */
    private function __queryParseReplace($key)
    {
        $aKey = ((int)$key[1] - 1);
        if (isset($this->_QueryParameters[$aKey]))
        {
            $var = $this->_QueryParameters[$aKey];
            if (is_string($var))
                return ("'".$this->_Driver->EscapeString($var)."'");
            else if (is_bool($var))
                return ($var?'1':'0');
            else if (is_array($var))
            {
                $s = '';
                foreach ($var as $item)
                {
                    if (is_string($item))
                        $s .= (",'" . $this->_Driver->EscapeString($item) . "'");
                    else
                        $s .= (',' . $this->_Driver->EscapeString($item));
                }
                $s[0] = '(';
                return ($s.')');
            }
            else
                return $this->_Driver->EscapeString($var);
        }

        return '';
    }

    /**
     * @param string $queryString
     * @param array $parameters
     * @return string
     */
    protected function ParseQuery($queryString,$parameters=array())
    {
        $argCount = count($parameters);
        if ($argCount > 0)
        {
            $this->_QueryParameters = $parameters;
            return preg_replace_callback(
                $this->_ParameterizedQueryPattern, array($this, '__queryParseReplace'), $queryString);
        }
        return $queryString;
    }

    /**
     * @param array $parameters
     * @return null|IDataTable|IDataTable[]
     * @throws Exception
     */
    public function GetDataWithArrayParameters($parameters)
    {
        $this->_SQL = $this->ParseQuery($this->_SQL,$parameters);
        $qResult = $this->_Driver->Query($this->_SQL);
        if ($qResult === false)
        {
            $error = $this->_Driver->GetError();
            DBI::Instance()->ThrowDatabaseException('Query execution has failed ('.$error.')');
        }

        return $this->GetDataTables($qResult,'Cannot retrieve result from query.');
    }

    /**
     * @param mixed[] $params
     * @return array|bool|mixed|null|resource
     * @throws Exception
     */
    public function Run(...$params)
    {
        return $this->RunWithArrayParameters($params);
    }

    /**
     * @param array $parameters
     * @return array|bool|mixed|null|resource
     * @throws Exception
     */
    public function RunWithArrayParameters($parameters)
    {
        $this->_SQL = $this->ParseQuery($this->_SQL,$parameters);
        $qResult = $this->_Driver->SingleQuery($this->_SQL);
        if ($qResult === false)
        {
            $error = $this->_Driver->GetError();
            DBI::Instance()->ThrowDatabaseException('Query execution has failed ('.$error.')');
        }

        if (is_bool($qResult))
            return $qResult;
        else if (is_integer($qResult))
            return ($qResult > 0);
        else
            DBI::Instance()->ThrowDatabaseException('Unknown query execution result. It is not boolean nor integer. ('.$qResult.')');
        return null;
    }
}
?>