<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Standard;
use Exception;
use RBS\Selifa\Data\Interfaces\IDataRow;
use RBS\Selifa\Data\Interfaces\IWritableDataRow;
use RBS\System\ITransferableObject;
use RBS\Selifa\Data\DBI;
use DateTime;

/**
 * Class ReadWriteDataRow
 *
 * @package RBS\Selifa\Data\Standard
 */
class ReadWriteDataRow extends BaseDataRow implements IWritableDataRow
{
    /**
     * @var int
     */
    protected $_RowMode;

    /**
     * @param bool $cancelSave
     */
    protected function OnSaveRow(&$cancelSave) { }

    /**
     *
     */
    protected function OnRowSaved() { }

    /**
     * @param bool $cancelDelete
     * @param bool $doUpdateInstead
     */
    public function OnDeleteRow(&$cancelDelete, &$doUpdateInstead) { }

    /**
     *
     */
    protected function OnRowDeleted() { }

    /**
     * @param int $rowMode
     * @return IWritableDataRow
     */
    public function SetRowMode($rowMode)
    {
        $this->_RowMode = $rowMode;
        return $this;
    }

    /**
     * @param string $name
     * @param string|float|int|bool|null $value
     */
    public function __set($name, $value)
    {
        if (array_key_exists($name, $this->_MetaData['ColumnInfo']))
        {
            $dialect = $this->_Driver->GetDialect();
            if (is_bool($value))
                $value = $dialect->TranslateBoolean($value);

            if ($this->_TCO != null)
            {
                $tObject = $this->_TCO->GetTransformation($name);
                if ($tObject != null)
                    $aValue = $tObject->ConvertFrom($value);
                else
                    $aValue = $value;
            }
            else $aValue = $value;

            if (($this->_MetaData['ColumnInfo'][$name]['value'] != $aValue) || ($aValue == ''))
            {
                if (($aValue !== DB_NULL) && ($this->_MetaData['ColumnInfo'][$name]['type'] != DBI_FT_TIMESTAMP))
                {
                    $this->_MetaData['ColumnInfo'][$name]['value'] = $aValue;
                    $this->_MetaData['ColumnInfo'][$name]['modified'] = true;
                }
                else
                    $this->_MetaData['ColumnInfo'][$name]['forcenull'] = true;
            }
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function Save()
    {
        $cancelSave = false;
        $this->OnSaveRow($cancelSave);
        if ($cancelSave)
            return false;

        $pairs = array();
        $validInput = true;

        $invalidColumnName = '';
        $invalidColumnData = '';
        $dialect = $this->_Driver->GetDialect();

        foreach ($this->_MetaData['ColumnInfo'] as $key => $value)
        {
            $vType = $value['type'];
            $notSkipped = (!$value['auto']) && ($vType != DBI_FT_TIMESTAMP) && (!$value['forcenull']);
            $isModified = true;
            if (isset($value['modified']))
                $isModified = (bool)$value['modified'];
            $isModified = ($isModified || ($this->_RowMode == DB_ROW_MODE_INSERT));
            if ($notSkipped && $isModified)
            {
                if ($value['value'] === null)
                {
                    //$pairs[$key] = null;
                }
                else
                {
                    if (($vType == DBI_FT_INTEGER) || ($vType == DBI_FT_REAL))
                    {
                        $validInput = is_numeric($value['value']);
                        $pairs[$key] = $value['value'];
                        if (!$validInput)
                        {
                            $invalidColumnName = $key;
                            $invalidColumnData = $value['value'];
                        }
                    }
                    else
                    {
                        if ($vType == DBI_FT_DATETIME)
                        {
                            if ($value['value'] instanceof DateTime)
                                $pairs[$key] = $value['value']->format($dialect->DateTimeFormat);
                            else
                            {
                                if (is_string($value['value']))
                                    $pairs[$key] = $this->_Driver->EscapeString($value['value']);
                                else
                                {
                                    if (is_integer($value['value']))
                                    {
                                        $dtObject = new DateTime();
                                        $dtObject->setTimestamp($value['value']);
                                        $pairs[$key] = $dtObject->format($dialect->DateTimeFormat);
                                    }
                                    else
                                    {
                                        $validInput = false;
                                        $invalidColumnName = $key;
                                        $invalidColumnData = $value['value'];
                                    }
                                }
                            }
                        }
                        else
                            $pairs[$key] = (string)$this->_Driver->EscapeString($value['value']);
                    }
                }
            }
            if (!$validInput)
                break;
        }

        if (!$validInput)
        {
            DBI::Instance()->ThrowDatabaseException('Invalid input value for ' . $invalidColumnName . ', value is: ' . $invalidColumnData . '.');
            return false;
        }

        if (count($pairs) <= 0)
            return true;

        if ($this->_RowMode == DB_ROW_MODE_INSERT)
        {
            $partA = '';
            $partB = '';
            foreach ($pairs as $cn => $cv)
            {
                $partA .= (','.$dialect->S.$cn.$dialect->E);
                if (is_string($cv))
                    $partB .= (",'".$cv."'");
                else
                    $partB .= (",".$cv);
            }

            $partA[0] = '(';
            $partB[0] = '(';
            $sql = ('INSERT INTO '.$dialect->S.$this->_MetaData['TableName'].$dialect->E.$partA.') VALUES'.$partB.')');
            $this->LastDMLQuery = $sql;
            $b = $this->_Driver->SingleQuery($sql);
            if (!$b)
            {
                $error = $this->_Driver->GetError();
                DBI::Instance()->ThrowDatabaseException('Insert query failed ('.$error.')', DB_INSERT_FAILED);
                return false;
            }

            if ($this->_MetaData['IsPrimaryKeyAvailable'])
            {
                $pKey = $this->_MetaData['PrimaryKeys'][0];
                if ($this->_MetaData['ColumnInfo'][$pKey]['auto'])
                {
                    $lastId = $this->_Driver->LastInsertedIdentifier();
                    $this->_MetaData['ColumnInfo'][$pKey]['value'] = $lastId;
                }
            }

            $this->OnRowSaved();
            return $b;
        }
        else
        {
            if (!$this->_MetaData['IsPrimaryKeyAvailable'])
            {
                DBI::Instance()->ThrowDatabaseException('No primary key defined.');
                return false;
            }

            $keyColumn = $this->_MetaData['PrimaryKeys'][0];
            $keyValue = $this->_MetaData['ColumnInfo'][$keyColumn]['value'];

            $sPart = '';
            foreach ($pairs as $cn => $cv)
                $sPart .= (','.$dialect->S.$cn.$dialect->E.' = '.(is_string($cv)?"'".$cv."'":$cv));
            $sPart[0] = ' ';

            $sql = ('UPDATE '.$dialect->S.$this->_MetaData['TableName'].$dialect->E.' SET'.$sPart.' WHERE ('.$dialect->S.$keyColumn.$dialect->E.' = '.(is_string($keyValue)?"'".$keyValue."'":$keyValue).')');
            $this->LastDMLQuery = $sql;
            $b = $this->_Driver->SingleQuery($sql);
            if (!$b)
            {
                $error = $this->_Driver->GetError();
                DBI::Instance()->ThrowDatabaseException('Update query failed ('.$error.')',DB_UPDATE_FAILED);
                return false;
            }

            $this->OnRowSaved();
            return $b;
        }
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function Delete()
    {
        if (!$this->_MetaData['IsPrimaryKeyAvailable'])
        {
            DBI::Instance()->ThrowDatabaseException('No primary key defined.');
            return false;
        }

        $cancelDelete = false;
        $doUpdateInstead = false;
        $this->OnDeleteRow($cancelDelete,$doUpdateInstead);
        if ($cancelDelete)
            return false;

        if ($doUpdateInstead)
        {
            return $this->Save();
        }
        else
        {
            $keyColumn = $this->_MetaData['PrimaryKeys'][0];
            $keyValue = $this->_MetaData['ColumnInfo'][$keyColumn]['value'];

            $d = $this->_Driver->GetDialect();
            $sql = ('DELETE FROM ' . $d->S . $this->_MetaData['TableName'] . $d->E . ' WHERE (' . $d->S . $keyColumn . $d->E . ' = ' . (is_string(
                    $keyValue) ? "'" . $keyValue . "'" : $keyValue) . ')');
            $this->LastDMLQuery = $sql;
            $b = $this->_Driver->SingleQuery($sql);
            if (!$b)
                DBI::Instance()->ThrowDatabaseException('Delete query failed.', DB_UPDATE_FAILED);

            $this->OnRowDeleted();
            return $b;
        }
    }

    /**
     * @param null|string[] $columns
     */
    public function DisableAutoValues($columns=null)
    {
        if ($columns === null)
        {
            foreach ($this->_MetaData['ColumnInfo'] as $key => $value)
                $this->_MetaData['ColumnInfo'][$key]['auto'] = false;
        }
        else
        {
            foreach ($columns as $key)
                $this->_MetaData['ColumnInfo'][$key]['auto'] = false;
        }
    }

    /**
     * @param ITransferableObject|array $tObjectOrArray
     */
    public function From($tObjectOrArray)
    {
        $dialect = $this->_Driver->GetDialect();
        if ($tObjectOrArray instanceof ITransferableObject)
        {
            $tMembers = $tObjectOrArray->GetTransferableMembers();
            foreach ($tMembers as $m)
            {
                $value = $tObjectOrArray->{$m['Name']};
                if (is_bool($value))
                    $this->{$m['Name']} = $dialect->TranslateBoolean($value);
                else
                    $this->{$m['Name']} = $value;
            }
        }
        else if (is_array($tObjectOrArray))
        {
            foreach ($tObjectOrArray as $key => $value)
            {
                if (is_bool($value))
                    $this->{$key} = $dialect->TranslateBoolean($value);
                else
                    $this->{$key} = $value;
            }
        }
    }
}
?>