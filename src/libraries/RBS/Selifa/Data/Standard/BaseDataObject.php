<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Standard;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use RBS\Selifa\Data\Interfaces\IDataExtension;
use RBS\Selifa\Data\Interfaces\IDataObject;
use RBS\Selifa\Data\Interfaces\IDataTable;
use RBS\Selifa\Data\DBI;
use Exception;
use SelifaException;

/**
 * Class BaseDataObject
 *
 * @package RBS\Selifa\Data\Standard
 */
abstract class BaseDataObject implements IDataObject
{
    /**
     * @var null|IDatabaseDriver
     */
    protected $_Driver = null;

    /**
     * @var null|IDataExtension
     */
    protected $_Extension = null;

    /**
     * @var array
     */
    protected $_DBOptions = [];

    /**
     * @var string
     */
    protected $_SQL = '';

    /**
     *
     */
    private function _TerminateMultiQuery()
    {
        if ($this->_Driver->IsMultiQuerySupported())
        {
            while ($this->_Driver->HasMoreResult())
                $this->_Driver->NextResult();
        }
    }

    /**
     * @param mixed|resource $qResult
     * @return IDataTable
     * @throws SelifaException
     */
    protected function CreateDataTableObject($qResult)
    {
        $dtcName = 'RBS\Selifa\Data\Standard\DataTable';
        if (isset($this->_DBOptions['DataTableClass']))
        {
            $cn = trim($this->_DBOptions['DataTableClass']);
            if ($cn != '')
                $dtcName = $cn;
        }

        $object = new $dtcName($this->_Driver,$this->_DBOptions,$qResult);
        if (!($object instanceof IDataTable))
            throw new SelifaException(DB_INVALID_CONFIGURATION,'Data table class ['.$dtcName.'] does not implement IDataTable.');
        return $object->SetExtension($this->_Extension);
    }

    /**
     * @param mixed|resource $qResult
     * @param string $errorMessage
     * @return null|IDataTable|IDataTable[]
     * @throws Exception
     */
    protected function GetDataTables($qResult,$errorMessage)
    {
        if (!$this->_Driver->IsMultiQuerySupported())
        {
            $this->DataTableCount = 1;
            return $this->CreateDataTableObject($qResult);
        }
        else
        {
            $dtCount = 0;
            $dtArray = [];
            while (true)
            {
                $aResult = $this->_Driver->GetResult();
                if ($aResult === false)
                {
                    if ($dtCount <= 0)
                        DBI::Instance()->ThrowDatabaseException($errorMessage);
                    else
                    {
                        $this->_Driver->NextResult();
                        break;
                    }
                }

                $dtArray[] = $this->CreateDataTableObject($aResult);
                $dtCount++;

                if (!$this->_Driver->HasMoreResult())
                {
                    $this->_Driver->NextResult();
                    break;
                }
                else
                    $this->_Driver->NextResult();
            }

            $this->DataTableCount = $dtCount;
            if ($dtCount <= 0)
                return null;
            else if ($dtCount == 1)
                return $dtArray[0];
            else
                return $dtArray;
        }
    }

    /**
     * @param mixed|resource $qResult
     * @return mixed|array|null
     */
    protected function GetFirstFetchedData($qResult)
    {
        $fetchedResult = null;
        if ($this->_Driver->IsMultiQuerySupported())
        {
            $storedResult = $this->_Driver->GetResult();
            if ($storedResult !== false)
            {
                $fetchedResult = $this->_Driver->Fetch($storedResult);
                $this->_Driver->FreeResult($storedResult);
            }
            $this->_TerminateMultiQuery();
        }
        else
        {
            $fetchedResult = $this->_Driver->Fetch($qResult);
            if ($fetchedResult !== false)
            {
                $this->_Driver->FreeResult($qResult);
                $this->_Driver->NextResult();
            }
            else
                $fetchedResult = null;
        }
        return $fetchedResult;
    }

    /**
     * @param IDataExtension $iExtension
     * @return IDataObject
     */
    public function SetExtension($iExtension)
    {
        $this->_Extension = $iExtension;
        return $this;
    }

    /**
     * @param mixed[] $params
     * @return null|IDataTable|IDataTable[]
     * @throws Exception
     */
    public function GetData(...$params)
    {
        return $this->GetDataWithArrayParameters($params);
    }

    /**
     * @param array $parameters
     * @return null|IDataTable|IDataTable[]
     * @throws Exception
     */
    public abstract function GetDataWithArrayParameters($parameters);

    /**
     * @return IDataObject
     */
    public function DisableExtension()
    {
        $this->_Extension = null;
        return $this;
    }
}
?>