<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Standard;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use RBS\Selifa\Data\Interfaces\IDataTable;
use RBS\Selifa\Data\Interfaces\IDataRow;
use RBS\Selifa\Data\DBI;
use Exception;
use RBS\Selifa\Data\Interfaces\IDataTransformCapability;
use RBS\Selifa\Data\Interfaces\ITableDataObject;
use RBS\Selifa\Data\Interfaces\IWritableDataRow;
use SelifaException;

/**
 * Class TableObject
 *
 * @package RBS\Selifa\Data\Standard
 */
class TableObject extends BaseDataObject implements ITableDataObject, IDataTransformCapability
{
    use DataTransformationCapabilityTrait;

    /**
     * @var string
     */
    protected $_ObjectName = '';

    /**
     * @var array
     */
    protected $_Columns = [];

    /**
     * @var null|array
     */
    protected $_TableMetaData = null;

    /**
     * @return string
     */
    private function __CreateDefaultTableQuery()
    {
        $dialect = $this->_Driver->GetDialect();
        $query = "select ";
        if (count($this->_Columns) > 0)
        {
            $glue = ($dialect->E.', '.$dialect->S);
            $query .= ($dialect->S.implode($glue,$this->_Columns).$dialect->E);
        }
        else
            $query .= "*";
        $query .= (" from ".$dialect->S.$this->_ObjectName.$dialect->E);
        return $query;
    }

    /**
     * @param array $aData
     * @return string
     */
    private function __ArrayToWhereQueryOnly($aData)
    {
        if (count($aData) > 0)
        {
            $dialect = $this->_Driver->GetDialect();
            $temps = [];
            foreach ($aData as $column => $value)
            {
                if (is_string($value))
                    $aValue = ("'".$this->_Driver->EscapeString($value)."'");
                else
                    $aValue = $this->_Driver->EscapeString($value);
                $temps[] = ($dialect->S.$column.$dialect->E.' = '.$aValue);
            }
            return ('where ('.implode(') AND (',$temps).')');
        }
        return '';
    }

    /**
     * @param array $filters
     */
    protected function OnFilterRow($filters) { }

    /**
     * @param IDataRow $row
     */
    protected function OnRowFetched($row) { }

    /**
     * @param IDataRow $row
     */
    protected function OnRowInserted($row) { }

    /**
     * @param IDatabaseDriver $driverObject
     * @param string $strCommand
     * @param array $dbOpts
     * @throws Exception
     */
    public function __construct($driverObject,$dbOpts,$strCommand)
    {
        $this->_Driver = $driverObject;
        $this->_DBOptions = $dbOpts;
        $this->_ObjectName = $strCommand;

        $this->_TableMetaData = $this->_Driver->GetResultMetadata(null,$strCommand);
        $this->RegisterDefaultTransformations($this->_TableMetaData);
    }

    /**
     * @return ITableDataObject
     */
    public function Select()
    {
        $args = func_get_args();
        if (count($args) > 0)
        {
            if (is_array($args[0]))
                $this->_Columns = $args[0];
            else
                $this->_Columns = $args;
        }
        return $this;
    }

    /**
     * @param array $parameters
     * @return null|IDataTable|IDataTable[]
     * @throws Exception
     */
    public function GetDataWithArrayParameters($parameters)
    {
        $this->_SQL = $this->__CreateDefaultTableQuery();
        $qResult = $this->_Driver->Query($this->_SQL);
        if ($qResult === false)
            DBI::Instance()->ThrowDatabaseException('Query execution has failed.');
        return $this->__GetDataTables($qResult,'Cannot retrieve result from specified table.');
    }

    /**
     * @param array|string|int $byIdOrFields
     * @return null|IWritableDataRow|IDataRow
     * @throws Exception
     */
    public function GetRow($byIdOrFields)
    {
        $filters = array();
        if (is_array($byIdOrFields))
            $filters = $byIdOrFields;
        else
        {
            if (!$this->_TableMetaData['IsPrimaryKeyAvailable'])
                DBI::Instance()->ThrowDatabaseException('Primary key is not exists for table '.$this->_ObjectName.'.');
            $firstPKName = $this->_TableMetaData['PrimaryKeys'][0];
            $filters[$firstPKName] = $byIdOrFields;
        }

        $this->OnFilterRow($filters);
        $sql = ($this->__CreateDefaultTableQuery() . ' ' . $this->__ArrayToWhereQueryOnly($filters));

        if ($this->_Driver->IsMultiQuerySupported())
        {
            $this->_Driver->Query($sql);
            $qResult = $this->_Driver->GetResult();
            $this->_Driver->NextResult();
        }
        else
            $qResult = $this->_Driver->Query($sql);

        $fetchedRow = $this->_Driver->Fetch($qResult);
        if ($fetchedRow === null)
            return null;
        $this->_Driver->FreeResult($qResult);

        $wtDrcName = 'RBS\Selifa\Data\Standard\ReadWriteDataRow';
        if (isset($this->_DBOptions['WritableRowClass']))
        {
            $temp = trim($this->_DBOptions['WritableRowClass']);
            if ($temp != '')
                $wtDrcName = $temp;
        }
        $fRow = new $wtDrcName($this->_Driver,$this->_DBOptions,$fetchedRow,$this->_TableMetaData);
        if (!($fRow instanceof IWritableDataRow))
            throw new SelifaException(DB_INVALID_CONFIGURATION,'Data row class ['.$wtDrcName.'] does not implement IWritableDataRow.');
        $fRow->SetExtension($this->_Extension);
        $fRow->SetTransformCapableObject($this);
        $fRow->SetRowMode(DB_ROW_MODE_UPDATE);

        $this->OnRowFetched($fRow);
        return $fRow;
    }

    /**
     * @param null|array $aData
     * @return IWritableDataRow
     * @throws Exception
     */
    public function NewRow($aData=null)
    {
        $wtDrcName = 'RBS\Selifa\Data\Standard\ReadWriteDataRow';
        if (isset($this->_DBOptions['WritableRowClass']))
        {
            $temp = trim($this->_DBOptions['WritableRowClass']);
            if ($temp != '')
                $wtDrcName = $temp;
        }
        $fRow = new $wtDrcName($this->_Driver,$this->_DBOptions,($aData==null?[]:$aData),$this->_TableMetaData);
        if (!($fRow instanceof IWritableDataRow))
            throw new SelifaException(DB_INVALID_CONFIGURATION,'Data row class ['.$wtDrcName.'] does not implement IWritableDataRow.');
        $fRow->SetExtension($this->_Extension);
        $fRow->SetTransformCapableObject($this);
        $fRow->SetRowMode(DB_ROW_MODE_INSERT);

        $this->OnRowInserted($fRow);
        return $fRow;
    }
}
?>