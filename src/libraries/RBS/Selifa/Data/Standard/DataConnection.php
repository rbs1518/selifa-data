<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Standard;
use Exception;
use RBS\Selifa\Data\Interfaces\IDataConnection;
use RBS\Selifa\Data\Interfaces\IDataExtension;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use RBS\Selifa\Data\Interfaces\IRunnableDataObject;
use RBS\Selifa\Data\Interfaces\ITableDataObject;
use RBS\Selifa\Data\BaseDialect;
use RBS\Selifa\Data\DBI;
use RBS\Selifa\Data\SQL\Statement;
use SelifaException;

/**
 * Class DataConnection
 *
 * @package RBS\Selifa\Data
 */
class DataConnection implements IDataConnection
{
    /**
     * @var null|IDatabaseDriver
     */
    protected $_Driver = null;

    /**
     * @var null|IDataExtension
     */
    protected $_Extension = null;

    /**
     * @var array
     */
    protected $_DBOptions = [];

    /**
     * @var string
     */
    public $DatabaseName = '';

    /**
     * @var string
     */
    public $DatabaseId;

    /**
     * @var bool
     */
    public $IsErrorDisable;

    /**
     * @var string
     */
    public $InterfaceType;

    /**
     * @var null|BaseDialect
     */
    public $Dialect = null;

    /**
     * DataConnection constructor.
     */
    public function __construct()
    {

    }

    /**
     *
     */
    function __destruct()
    {
        $this->_Driver->Close();
    }

    /**
     * @param string $dbId
     * @param string $driverCn
     * @param array $dbConfig
     * @param bool $disableError
     * @throws Exception
     */
    public function Initialize($dbId,$driverCn,$dbConfig,$disableError)
    {
        $drvObject = new $driverCn;
        if (!($drvObject instanceof IDatabaseDriver))
            DBI::Instance()->ThrowDatabaseException("'".$driverCn."' does not implement IDatabaseDriver");
        $drvObject->Initialize($dbConfig);
        $this->_Driver = $drvObject;
        $this->_DBOptions = $dbConfig;

        $this->DatabaseId = $dbId;
        $this->IsErrorDisable = $disableError;
        $this->InterfaceType = $drvObject->GetDriverType();
        $this->DatabaseName = $drvObject->GetDatabaseName();
        $this->Dialect = $drvObject->GetDialect();
    }

    /**
     * @param IDataExtension $iExtension
     */
    public function SetExtension(IDataExtension $iExtension)
    {
        $this->_Extension = $iExtension;
    }

    /**
     * @return null|IDataExtension
     */
    public function GetExtensions()
    {
        return $this->_Extension;
    }

    /**
     * @return IDatabaseDriver
     */
    public function GetDriver()
    {
        return $this->_Driver;
    }

    /**
     * @param string $sqlCommand
     * @return IRunnableDataObject
     * @throws Exception
     */
    public function Prepare($sqlCommand)
    {
        $docName = 'RBS\Selifa\Data\Standard\QueryObject';
        if (isset($this->_DBOptions['QueryObjectClass']))
        {
            $temp = trim($this->_DBOptions['QueryObjectClass']);
            if ($temp != '')
                $docName = $temp;
        }

        $do = new $docName($this->_Driver,$this->_DBOptions,$sqlCommand);
        if (!($do instanceof IRunnableDataObject))
            throw new SelifaException(DB_INVALID_CONFIGURATION,'Data object class ['.$docName.'] does not implement IRunnableDataObject.');
        $do->SetExtension($this->_Extension);
        return $do;
    }

    /**
     * @param string $tableName
     * @return ITableDataObject
     * @throws Exception
     */
    public function GetTable($tableName)
    {
        $docName = 'RBS\Selifa\Data\Standard\TableObject';
        if (isset($this->_DBOptions['TableObjectClass']))
        {
            $temp = trim($this->_DBOptions['TableObjectClass']);
            if ($temp != '')
                $docName = $temp;
        }

        $do = new $docName($this->_Driver,$this->_DBOptions,$tableName);
        if (!($do instanceof ITableDataObject))
            throw new SelifaException(DB_INVALID_CONFIGURATION,'Data object class ['.$docName.'] does not implement ITableDataObject.');
        $do->SetExtension($this->_Extension);
        return $do;
    }

    /**
     * @param string $procedureName
     * @return IRunnableDataObject
     * @throws Exception
     */
    public function UseProcedure($procedureName)
    {
        $docName = 'RBS\Selifa\Data\Standard\ProcedureObject';
        if (isset($this->_DBOptions['ProcedureObjectClass']))
        {
            $temp = trim($this->_DBOptions['ProcedureObjectClass']);
            if ($temp != '')
                $docName = $temp;
        }

        $do = new $docName($this->_Driver,$this->_DBOptions,$procedureName);
        if (!($do instanceof IRunnableDataObject))
            throw new SelifaException(DB_INVALID_CONFIGURATION,'Data object class ['.$docName.'] does not implement IRunnableDataObject.');
        $do->SetExtension($this->_Extension);
        return $do;
    }

    /**
     * @param string $input
     * @return string
     */
    public function EscapeString($input)
    {
        return $this->_Driver->EscapeString($input);
    }

    /**
     *
     */
    public function BeginTransaction()
    {
        $this->_Driver->TransactionStart();
    }

    /**
     * @param bool $cancel
     */
    public function EndTransaction($cancel=false)
    {
        if ($cancel)
            $this->_Driver->TransactionRollback();
        else
            $this->_Driver->TransactionCommit();
    }

    /**
     * @param string $sql
     * @param int $type
     * @return Statement
     */
    public function CreateStatement($sql='',$type=1)
    {
        return new Statement($this,$sql,$type);
    }

    /**
     *
     */
    public function Close()
    {
        $this->_Driver->Close();
    }
}
?>