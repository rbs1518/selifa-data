<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Standard;
use RBS\Selifa\Data\Interfaces\IRowTransformation;
use RBS\Selifa\Data\Transformation\DBBitToBoolean;
use RBS\Selifa\Data\Transformation\DBDoubleToDouble;
use RBS\Selifa\Data\Transformation\DBIntToInteger;

/**
 * Trait DataTransformationCapabilityTrait
 *
 * @package RBS\Selifa\Data\Standard
 */
trait DataTransformationCapabilityTrait
{
    /**
     * @var array
     */
    protected $_Transformations = [];

    /**
     * @param array metadata
     */
    protected function RegisterDefaultTransformations($metadata)
    {
        $i = new DBIntToInteger();
        $b = new DBBitToBoolean();
        $d = new DBDoubleToDouble();
        foreach ($metadata['ColumnInfo'] as $key => $spec)
        {
            if ($spec['ptype'] == DBI_FT_INTEGER)
                $this->_Transformations[$key] = $i;
            else if ($spec['ptype'] == DBI_FT_REAL)
                $this->_Transformations[$key] = $d;
            else if ($spec['ptype'] == DBI_FT_BOOLEAN)
                $this->_Transformations[$key] = $b;
        }
    }

    /**
     * @param IRowTransformation[] $tObjects
     * @return static
     */
    public function RegisterTransformations($tObjects)
    {
        $this->_Transformations = array_replace_recursive($this->_Transformations,$tObjects);
        return $this;
    }

    /**
     * @param string $key
     * @return IRowTransformation|null
     */
    public function GetTransformation($key)
    {
        if (isset($this->_Transformations[$key]))
            return $this->_Transformations[$key];
        else
            return null;
    }
}
?>