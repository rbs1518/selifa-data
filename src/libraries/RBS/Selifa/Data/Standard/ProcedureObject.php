<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Standard;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use RBS\Selifa\Data\Interfaces\IRunnableDataObject;
use RBS\Selifa\Data\Interfaces\IDataTable;
use RBS\Selifa\Data\DBI;
use Exception;

/**
 * Class ProcedureObject
 *
 * @package RBS\Selifa\Data\Standard
 */
class ProcedureObject extends BaseDataObject implements IRunnableDataObject
{
    /**
     * @var string
     */
    private $_ObjectName = '';

    /**
     * @param IDatabaseDriver $driverObject
     * @param string $strCommand
     * @param array $dbOpts
     * @throws Exception
     */
    public function __construct($driverObject,$dbOpts,$strCommand)
    {
        $this->_Driver = $driverObject;
        $this->_DBOptions = $dbOpts;

        $dialect = $this->_Driver->GetDialect();
        $this->_ObjectName = ($dialect->S.$strCommand.$dialect->E);
    }

    /**
     * @param mixed[] $params
     * @return null|IDataTable|IDataTable[]
     * @throws Exception
     */
    public function GetData(...$params)
    {
        return $this->GetDataWithArrayParameters($params);
    }

    /**
     * @param array $parameters
     * @return null|IDataTable|IDataTable[]
     * @throws Exception
     */
    public function GetDataWithArrayParameters($parameters)
    {
        $this->_SQL = $this->_Driver->StoredProcedureSQL($this->_ObjectName,$parameters);
        $qResult = $this->_Driver->Query($this->_SQL);
        if ($qResult === false)
        {
            $error = $this->_Driver->GetError();
            //DBI::Instance()->ThrowDatabaseException('Cannot execute stored procedure: '.$this->_ObjectName.' ('.$error.')');
            DBI::Instance()->ThrowDatabaseException('Cannot execute stored procedure ('.$error.')');
        }

        return $this->GetDataTables($qResult,'Cannot retrieve result from stored procedure.');
    }

    /**
     * @param mixed[] $params
     * @return array|bool|mixed|null|resource
     * @throws Exception
     */
    public function Run(...$params)
    {
        return $this->RunWithArrayParameters($params);
    }

    /**
     * @param array $parameters
     * @return array|bool|mixed|null|resource
     * @throws Exception
     */
    public function RunWithArrayParameters($parameters)
    {
        $this->_SQL = $this->_Driver->StoredProcedureSQL($this->_ObjectName,$parameters);
        $qResult = $this->_Driver->Query($this->_SQL);
        if ($qResult === false)
        {
            $error = $this->_Driver->GetError();
            //DBI::Instance()->ThrowDatabaseException('Cannot execute stored procedure: '.$this->_ObjectName.' ('.$error.')');
            DBI::Instance()->ThrowDatabaseException('Cannot execute stored procedure ('.$error.')');
        }
        return $this->GetFirstFetchedData($qResult);
    }
}
?>