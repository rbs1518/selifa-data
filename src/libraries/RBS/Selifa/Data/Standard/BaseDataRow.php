<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Standard;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use RBS\Selifa\Data\Interfaces\IDataRow;
use RBS\Selifa\Data\Interfaces\IDataExtension;
use RBS\Selifa\Data\Interfaces\IDataTransformCapability;
use RBS\System\ITransferableObject;

/**
 * Class BaseDataRow
 *
 * @package RBS\Selifa\Data\Standard
 */
abstract class BaseDataRow implements IDataRow
{
    /**
     * @var null|IDatabaseDriver
     */
    protected $_Driver = null;

    /**
     * @var null|IDataExtension
     */
    protected $_Extension = null;

    /**
     * @var null|array
     */
    protected $_MetaData = null;

    /**
     * @var null|IDataTransformCapability
     */
    protected $_TCO = null;

    /**
     * @var array
     */
    protected $_DBOptions = [];

    /**
     * @param IDatabaseDriver $driverObject
     * @param array $dbOpts
     * @param array $rowValues
     * @param array $metaData
     */
    public function __construct($driverObject,$dbOpts,$rowValues,$metaData)
    {
        $this->_Driver = $driverObject;
        $this->_MetaData = $metaData;
        $this->_DBOptions = $dbOpts;
        if ($rowValues != null)
        {
            foreach ($rowValues as $key => $value )
            {
                if (isset($this->_MetaData['ColumnInfo'][$key]))
                    $this->_MetaData['ColumnInfo'][$key]['value'] = $value;
            }
        }
    }

    /**
     * @param IDataExtension $iExtension
     * @return IDataRow
     */
    public function SetExtension($iExtension)
    {
        $this->_Extension = $iExtension;
        return $this;
    }

    /**
     * @param IDataTransformCapability $object
     * @return IDataRow
     */
    public function SetTransformCapableObject($object)
    {
        $this->_TCO = $object;
        return $this;
    }

    /**
     * @return array
     */
    public function GetColumnNames()
    {
        $result = array();
        foreach ($this->_MetaData['ColumnInfo'] as $key => $content)
            $result[] = $key;
        return $result;
    }

    /**
     * @param string $name
     * @return string|float|int|bool|null
     */
    public function __get($name)
    {
        if (array_key_exists($name, $this->_MetaData['ColumnInfo']))
        {
            if ($this->_TCO != null)
            {
                $value = $this->_MetaData['ColumnInfo'][$name]['value'];
                $tObject = $this->_TCO->GetTransformation($name);
                if ($tObject != null)
                    return $tObject->ConvertTo($value);
                else
                    return $value;
            }
            else
                return $this->_MetaData['ColumnInfo'][$name]['value'];
        }
        return null;
    }

    /**
     * @return string|float|int|bool|null
     */
    public function FirstColumnValue()
    {
        $temp = array_keys($this->_MetaData['ColumnInfo']);
        if (count($temp) > 0)
        {
            if ($this->_TCO != null)
            {
                $value = $this->Columns[$temp[0]]['value'];
                $tObject = $this->_TCO->GetTransformation($temp[0]);
                if ($tObject != null)
                    return $tObject->ConvertTo($value);
                else
                    return $value;
            }
            else
                return $this->Columns[$temp[0]]['value'];
        }
        else
            return null;
    }

    /**
     * @return array
     */
    public function ToArray()
    {
        $res = array();
        foreach ($this->_MetaData['ColumnInfo'] as $key => $val)
            $res[$key] = $this->{$key};
        return $res;
    }

    #region ITransferableObject implementation(s)
    /**
     * @return array
     */
    public function GetTransferableMembers()
    {
        $check = array(DBI_FT_BLOB,DBI_FT_DATETIME,DBI_FT_TIMESTAMP,DBI_FT_VARCHAR);
        $result = array();
        foreach ($this->_MetaData['ColumnInfo'] as $key => $item)
        {
            $result[] = array(
                'Name' => $key,
                'DataType' => (in_array($item['type'],$check)?'string':$item['type'])
            );
        }
        return $result;
    }

    /**
     * @param ITransferableObject|array $tObjectOrArray
     */
    public function To(&$tObjectOrArray)
    {
        if ($tObjectOrArray instanceof ITransferableObject)
        {
            $tMembers = $tObjectOrArray->GetTransferableMembers();
            foreach ($tMembers as $m)
            {
                $value = $this->{$m['Name']};
                if ($m['DataType'] == 'bool')
                    $tObjectOrArray->{$m['Name']} = ($value == '1');
                else if ($m['DataType'] == 'int')
                    $tObjectOrArray->{$m['Name']} = (int)$value;
                else if ($m['DataType'] == 'float')
                    $tObjectOrArray->{$m['Name']} = (float)$value;
                else if ($m['DataType'] == 'double')
                    $tObjectOrArray->{$m['Name']} = (double)$value;
                else
                    $tObjectOrArray->{$m['Name']} = $value;
            }
        }
        else if (is_array($tObjectOrArray))
        {
            foreach ($this->_MetaData['ColumnInfo'] as $key => $item)
                $tObjectOrArray[$key] = $this->{$key};
        }
    }
    #endregion

    #region ITGXObjectRenderer implementation
    /**
     * @param object $tgxObject
     * @return mixed|array
     */
    public function TGXRender($tgxObject)
    {
        return $this->ToArray();
    }
    #endregion

    #region JsonSerializable implementation
    /**
     *
     */
    public function jsonSerialize()
    {
        return $this->ToArray();
    }
    #endregion
}
?>