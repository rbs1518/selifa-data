<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Selifa\Data\Standard;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use RBS\Selifa\Data\Interfaces\IDataRow;
use RBS\Selifa\Data\Interfaces\IDataTable;
use RBS\Selifa\Data\Interfaces\IDataTransformCapability;
use RBS\Selifa\Data\Interfaces\IDataExtension;
use RBS\Selifa\Data\Interfaces\IRowTransformation;
use RBS\Selifa\Data\DBI;
use Exception;
use SelifaException;

/**
 * Class DataTable
 * @package RBS\Selifa\Data
 */
class DataTable implements IDataTable, IDataTransformCapability
{
    use DataTransformationCapabilityTrait;

    /**
     * @var null|IDatabaseDriver
     */
    protected $_Driver = null;

    /**
     * @var null|IDataExtension
     */
    protected $_Extension = null;

    /**
     * @var array
     */
    protected $_DBOptions = [];

    /**
     * @var null|resource|mixed
     */
    protected $_ResultInstance = null;

    /**
     * @var bool
     */
    protected $_IsFree = false;

    /**
     * @var null|array
     */
    protected $_MetaData = null;

    /**
     * @var bool
     */
    protected $_UseNumberedRow = false;

    /**
     * @var string
     */
    protected $_RowNumberColumnName = '';

    /**
     * @var int
     */
    protected $_CurrentRowNo = 1;

    /**
     * @param array $fetchedRow
     * @return IDataRow
     * @throws SelifaException
     */
    protected function CreateDataRowObject($fetchedRow)
    {
        $roDrcName = 'RBS\Selifa\Data\Standard\ReadOnlyDataRow';
        if (isset($this->_DBOptions['ReadOnlyRowClass']))
        {
            $temp = trim($this->_DBOptions['ReadOnlyRowClass']);
            if ($temp != '')
                $roDrcName = $temp;
        }
        $fRow = new $roDrcName($this->_Driver,$this->_DBOptions,$fetchedRow,$this->_MetaData);
        if (!($fRow instanceof IDataRow))
            throw new SelifaException(DB_INVALID_CONFIGURATION,'Data row class ['.$roDrcName.'] does not implement IDataRow.');
        $fRow->SetExtension($this->_Extension);
        $fRow->SetTransformCapableObject($this);
        return $fRow;
    }

    /**
     * @param IDataRow $row
     */
    protected function OnRowFetched($row) { }

    /**
     * @param IDatabaseDriver $driverObject
     * @param array $dbOpts
     * @param resource|mixed $resultInstance
     */
    public function __construct($driverObject,$dbOpts,$resultInstance)
    {
        $this->_Driver = $driverObject;
        $this->_ResultInstance = $resultInstance;
        $this->_DBOptions = $dbOpts;
        $this->_MetaData = $driverObject->GetResultMetadata($resultInstance,'');
        $this->RegisterDefaultTransformations($this->_MetaData);
    }

    /**
     *
     */
    public function __destruct()
    {
        if (!$this->_IsFree)
            $this->_Driver->FreeResult($this->_ResultInstance);
    }

    /**
     * @param IDataExtension $iExtension
     * @return IDataTable
     */
    public function SetExtension($iExtension)
    {
        $this->_Extension = $iExtension;
        return $this;
    }

    /**
     * @param bool $processRow
     * @return array
     * @throws Exception
     */
    public function ToArray($processRow=true)
    {
        if ($this->_IsFree)
            DBI::Instance()->ThrowDatabaseException('Cannot fetch data due to resultset has been released.');
        $result = array();

        if ($processRow)
        {
            $rowNo = 1;
            while ($row = $this->Fetch())
            {
                $resultRow = array();
                if ($this->_UseNumberedRow)
                    $resultRow[$this->_RowNumberColumnName] = $rowNo;
                foreach ($this->_MetaData['ColumnInfo'] as $key => $md)
                    $resultRow[$key] = $row->{$key};
                $result[] = $resultRow;
            }
        }
        else
        {
            $rowNo = 1;
            while ($row = $this->_Driver->Fetch($this->_ResultInstance))
            {
                if ($this->_UseNumberedRow)
                    $row[$this->_RowNumberColumnName] = $rowNo;
                $result[] = $row;
                $rowNo++;
            }
            $this->_Driver->FreeResult($this->_ResultInstance);
            $this->_IsFree = true;
        }
        return $result;
    }

    /**
     * @return bool|IDataRow
     * @throws Exception
     */
    public function Fetch()
    {
        if ($this->_IsFree)
            DBI::Instance()->ThrowDatabaseException('Cannot fetch data due to resultset has been released.');

        $fetchedRow = $this->_Driver->Fetch($this->_ResultInstance);
        if ($fetchedRow === null)
        {
            $this->_Driver->FreeResult($this->_ResultInstance);
            $this->_IsFree = true;
            return false;
        }

        if ($this->_UseNumberedRow)
            $fetchedRow[$this->_RowNumberColumnName] = $this->_CurrentRowNo;

        $fRow = $this->CreateDataRowObject($fetchedRow);
        $this->OnRowFetched($fRow);

        $this->_CurrentRowNo++;
        return $fRow;
    }

    /**
     * @param callable|null $rowCallback
     * @param bool $toDataRow
     * @throws Exception
     */
    public function Each($rowCallback,$toDataRow=true)
    {
        if ($this->_IsFree)
            DBI::Instance()->ThrowDatabaseException('Cannot fetch data due to resultset has been released.');
        if (!is_callable($rowCallback))
            DBI::Instance()->ThrowDatabaseException('rowCallback is not a callable type or null.');

        $rowNo = 1;
        while ($dbRow = $this->_Driver->Fetch($this->_ResultInstance))
        {
            if ($this->_UseNumberedRow)
                $dbRow[$this->_RowNumberColumnName] = $rowNo;
            if ($toDataRow)
            {
                $dataRow = $this->CreateDataRowObject($dbRow);
                $this->OnRowFetched($dataRow);
                $rowCallback($dbRow, $dataRow);
            }
            else
                $rowCallback($dbRow, null);
            $rowNo++;
        }

        $this->_Driver->FreeResult($this->_ResultInstance);
        $this->_IsFree = true;
    }

    /**
     * @return IDataRow|bool
     * @throws Exception
     */
    public function FirstRow()
    {
        if ($this->_IsFree)
            DBI::Instance()->ThrowDatabaseException('Cannot fetch data due to resultset has been released.');

        $dbRow = $this->_Driver->Fetch($this->_ResultInstance);
        if ($dbRow == false)
            return null;

        $dataRow = $this->CreateDataRowObject($dbRow);
        $this->_Driver->FreeResult($this->_ResultInstance);
        $this->_IsFree = true;

        $this->OnRowFetched($dataRow);
        return $dataRow;
    }

    /**
     * @param string $columnName
     * @return IDataTable
     */
    public function WithRowNumbers($columnName='RowNo')
    {
        $this->_UseNumberedRow = true;
        $this->_RowNumberColumnName = $columnName;
        $this->_MetaData['ColumnInfo'][$columnName] = array(
            'value' => null,
            'name' => $columnName,
            'type' => 'int',
            'key' => false,
            'auto' => false,
            'modified' => false,
            'forcenull' => false
        );
        return $this;
    }

    /**
     * @return IDataTable
     */
    public function ResetNumbers()
    {
        $this->_CurrentRowNo = 1;
        return $this;
    }

    /**
     * @param array|string $sourceOrKey
     * @param IRowTransformation|null $tObject
     * @return IDataTable
     */
    public function RegisterTransformation($sourceOrKey,$tObject=null)
    {
        if (is_array($sourceOrKey))
            $this->_Transformations = array_replace_recursive($this->_Transformations,$sourceOrKey);
        else if (($tObject != null) && ($tObject instanceof IRowTransformation))
            $this->_Transformations[$sourceOrKey] = $tObject;
        return $this;
    }

    /**
     * @param string $key
     * @return IRowTransformation|null
     */
    public function GetTransformation($key)
    {
        if (isset($this->_Transformations[$key]))
            return $this->_Transformations[$key];
        else
            return null;
    }

    /**
     * @return array|null
     */
    public function GetMetaData()
    {
        return $this->_MetaData;
    }

    /**
     * @return string
     */
    public function GetTableName()
    {
        return trim($this->_MetaData['TableName']);
    }

    /**
     * @return array
     */
    public function GetColumnInfo()
    {
        return $this->_MetaData['ColumnInfo'];
    }

    #region ITGXObjectRenderer implementation
    /**
     * @param object $tgxObject
     * @return array
     * @throws Exception
     */
    public function TGXRender($tgxObject)
    {
        $result = array();
        while ($row = $this->Fetch())
            $result[] = $row->ToArray();
        return $result;
    }
    #endregion

    #region JsonSerializable implementation
    /**
     *
     */
    public function jsonSerialize()
    {
        $result = array();
        while ($row = $this->Fetch())
            $result[] = $row->ToArray();
        return $result;
    }
    #endregion
}
?>