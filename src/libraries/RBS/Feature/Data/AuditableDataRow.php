<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Feature\Data;
use RBS\Selifa\Data\Standard\ReadWriteDataRow;
use DateTime;

/**
 * Class AuditableDataRow
 * @package RBS\Feature\Data
 */
class AuditableDataRow extends ReadWriteDataRow
{
    /**
     * @param bool $cancelSave
     */
    protected function OnSaveRow(&$cancelSave)
    {
        $cUserId = 0;
        if ($this->_Extension instanceof AuditableDataExtension)
            $cUserId = $this->_Extension->GetUserID();

        if ($this->_RowMode == DB_ROW_MODE_INSERT)
        {
            $this->CUID = $cUserId;
            $this->MUID = $cUserId;
            $this->Created = new DateTime();
        }
        else if ($this->_RowMode = DB_ROW_MODE_UPDATE)
        {
            $this->MUID = $cUserId;
        }
    }

    /**
     * @param bool $cancelDelete
     * @param bool $doUpdateInstead
     */
    public function OnDeleteRow(&$cancelDelete, &$doUpdateInstead)
    {
        $this->DFlag = 1;
        $doUpdateInstead = true;
    }
}
?>