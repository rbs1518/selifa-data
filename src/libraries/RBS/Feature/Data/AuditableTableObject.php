<?php
/*
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This software is licensed under the MIT license. For more information,
 * see LICENSE.
 */

namespace RBS\Feature\Data;
use RBS\Selifa\Data\Standard\TableObject;
use RBS\Selifa\Data\Interfaces\IDatabaseDriver;
use Exception;

/**
 * Class AuditableTableObject
 *
 * @package RBS\Feature\Data
 */
class AuditableTableObject extends TableObject
{
    /**
     * @var bool
     */
    private $_IsExcludedFromAudit = false;

    /**
     * @param IDatabaseDriver $driverObject
     * @param string $strCommand
     * @param array $dbOpts
     * @throws Exception
     */
    public function __construct($driverObject,$dbOpts,$strCommand)
    {
        parent::__construct($driverObject, $dbOpts, $strCommand);
        if (isset($this->_DBOptions['ExcludedFromAudit']))
            $this->_IsExcludedFromAudit = in_array($this->_ObjectName,$this->_DBOptions['ExcludedFromAudit']);

        if ($this->_IsExcludedFromAudit)
            $this->_DBOptions['WritableRowClass'] = '';
    }

    /**
     * @param array $filters
     */
    protected function OnFilterRow($filters)
    {
        if (!$this->_IsExcludedFromAudit)
            $filters['DFlag'] = 0;
    }
}
?>